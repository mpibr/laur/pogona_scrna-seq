#plotting for the manuscript

#loading libraries
library(Seurat)
library(ggplot2)
library(corrplot)

###preprocessing
#reading in Seurat and other objects and subset some
cells <- readRDS(file="../data/Pogona_vitticeps_cells.rds")
neurons <- readRDS(file="../data/neurons_SCTransform_annotated.rds")
mouse_lizard_CCA_integrated <- readRDS(file ="../data/brain_integrated_annotated_CCA_clustered_and_subclustered.rds")
neurons_v3 <- subset(neurons, subset = chemistry == "v3")
l6_r2_cns_neurons <- readRDS(file="../data/l6_r2_cns_neurons_Seurat.rds")
label_transfer_matrix_mus_reference <- readRDS(file="../data/liz_query_mouse_reference_normalized_ClusterID_vargenes_1500_dims_40.rds")
#ordered list of mouse and lizard clusters
mus_names <- c(paste0("OBDOP", seq(from=1, to=2)), paste0("OBINH", seq(from=1, to=5)), paste0("OBNBL", seq(from=1, to=5)), paste0("TEGLU", seq(from=1, to=24)), "CR", paste0("DGGRC", seq(from=1, to=2)), paste0("DGNBL", seq(from=1, to=2)), paste0("TEINH", seq(from=1, to=21)), paste0("MSN", seq(from=1, to=6)), "SZNBL", "SEPNBL", paste0("DEGLU", seq(from=1, to=5)),"DETPH", "DECHO2", paste0("HYPEP", seq(from=1, to=8)), paste0("DEINH", seq(from=1, to=8)), paste0("MEGLU", seq(from=1, to=14)), paste0("MEINH", seq(from=1, to=14)), "CBGRC", paste0("CBINH", seq(from=1, to=2)), "CBPC", paste0("CBNBL", seq(from=1, to=2)), paste0("HBGLU", seq(from=1, to=10)), paste0("HBINH", seq(from=1, to=9)), paste0("SCGLU", seq(from=1, to=10)), paste0("SCINH", seq(from=1, to=11)), "TECHO", "DECHO1", "MBCHO1",paste0("HBCHO", seq(from=1, to=4)), paste0("MBDOP", seq(from=1, to=2)), paste0("HBSER", seq(from=1, to=5)), "HBADR", "HBNOR")
liz_names_anatomical <- c(paste0("TEGLUT", seq(from=1, to=29)),paste0("TEGABA", seq(from=1, to=24)),paste0("DIGLUT", seq(from=1, to=55)),paste0("DIGABAGLUT", seq(from=1, to=8)),paste0("DICHOL", seq(from=1, to=3)),paste0("DISECR", seq(from=1, to=7)),paste0("DIGABA", seq(from=1, to=34)),paste0("MEGLUT", seq(from=1, to=13)),paste0("MEGABA", seq(from=1, to=45)),paste0("CBGLUT", seq(from=1, to=5)),paste0("CBGABA", seq(from=1, to=2)),"RHGABA1","TECHOL1",paste0("DIDOPA", seq(from=1, to=2)),paste0("DISERT", seq(from=1, to=2)),"MESERT1","RHCHOL1")
dev_comp_max_LT <- readRDS(file="../data/maximum_label_transfer_score_by_cluster_developmental_compartment.rds") #maximum label transfer score by developmental compartment
INs_integrated <- readRDS(file="../data/INs_integrated_clustered.rds")
INs_integrated_red <- readRDS(file="../data/INs_integrated_red.rds")
liz_TEGABA <- readRDS(file="/gpfs/laur/experiments/scRNA-seq/pogona_scrna-seq/comparative_analyses/allen_institute_cortex/SCTransform_20211008/liz_INs.rds")
SC_integrated <- readRDS(file ="../data/SC_integrated_clustered.rds")
hyp_integrated <- readRDS(file = "../data/hypothalamus_integrated_clustered.rds")
liz_hyp_neurons <- readRDS(file="../data/liz_hyp_neurons.rds")
liz_thal <- readRDS(file="../data/thalamus_subclustered.rds")
thal_saunders_integrated <- readRDS(file="../data/thal_saunders_integrated_clustered.rds")
thal_integrated <- readRDS(file="../data/thalamus_integrated_Saunders_Phillips_Zeisel_lizard_clustered.rds")
thal_glu <- readRDS(file="../data/thal_glu_PCA_calc.rds")
mouse_thalamus_integrated <- readRDS(file ="../data/thalamus_mouse_integrated.rds")


#reading hierarchical clustering dendrogram and re-ordering neuronal clusters
dend <- readRDS("../data/pvclust.TFs_dist_dendrogram_rotated.rds")
dendrogram_names <- labels(dend)
levels(neurons) <- dendrogram_names

#extracting UMAP coordinates from lizard-mouse-CCA-integrated object and transferring them to SCTransformed mouse and lizard objects
bi_umap <- Embeddings(mouse_lizard_CCA_integrated, reduction = "umap")
l6_r2_cns_neurons <- SCTransform(l6_r2_cns_neurons, return.only.var.genes = FALSE)
neurons_v3 <- SCTransform(neurons_v3, vars.to.regress=c("animal"), return.only.var.genes = FALSE)
l6_r2_cns_neurons$species <- "mouse"
neurons_v3$species <- "lizard"
DefaultAssay(l6_r2_cns_neurons) <- "SCT"
DefaultAssay(neurons_v3) <- "SCT"
mus_liz_v3 <- merge(l6_r2_cns_neurons,neurons_v3)
mus_liz_v3[["umap"]]<- CreateDimReducObject(embeddings = bi_umap, key = "UMAP_", assay = DefaultAssay(mus_liz_v3))
DefaultAssay(mus_liz_v3) <- "SCT"
mus <- subset(mus_liz_v3, subset = species == "mouse")
liz <- subset(mus_liz_v3, subset = species == "lizard")



####Main figures
##Fig. 1
#Fig. 1B/S2B
png("./plots/cells_DimPlot_fig_1B-S2B.png",height=2048,width=2048)
DimPlot(object = cells, reduction ="umap", label=F, pt.size=0.01) + NoLegend()
dev.off()

#Fig. 1C
Idents(cells) <- factor(Idents(cells), levels=rev(c("neurons","NPC","e-glia","glia_cerebellum","OPC","diff_OPC","oligodendrocytes","microglia","SCO","choroid_plexus","vEnd","vSMC","End","RBC","cycl_cells","JCHAIN")))
genes <- c("SNAP25","SLC17A6","SLC32A1","SOX4","SOX11","GFAP","DRAXIN","PDGFRA","GPR17","MAG","C1QC","SSPO","TTR","SOX18","ACTA2","COL1A1","CD24","MKI67","JCHAIN")
pdf("./plots/cells_DotPlot_fig_1C.pdf",width = 8)
DotPlot(cells, features = rev(genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()

#Fig. 1D
col <- rev(c("#FF63B6","#EF67EB","#B385FF","#00A6FF","#00BADE","#00C1A7","#00BD5C","#64B200","#AEA200","#DB8E00","#F8766D"))
png("./plots/neurons_DimPlot_assigned_regions_fig_1D.png",height=1024,width=1024)
DimPlot(object = neurons, reduction ="umap",group.by = "assigned_region",label=T,repel=T, pt.size=1,cols=col)
dev.off()

#Fig. 1E --> see ../analysis/cumulative_gene_family_expression_by_cluster.R

#Fig. 1F
levels(neurons) <- rev(dendrogram_names)
genes <- c("FOXG1","ZBTB18", "MEF2C", "TBR1","NFIX", "ZEB2", "MEIS2", "NR2F2","DACH2", "ETV1", "DLX5", "ARX","LHX1", "LHX6", "LHX8", "LHX9","FOXP1", "FOXP2","FOXP4", "ZIC1","NKX2-1","ISL1","SIX3", "OTP","SIM1","NR5A1" ,"TCF7L2", "GBX2", "PROX1", "POU4F1","OTX2", "GATA3","SOX14","PAX6","PAX7", "TAL1")
pdf("./plots/neurons_dotplot_TFs_fig_1F.pdf",height = 60,width=11)
DotPlot(neurons, features = rev(genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()
ggsave("./plots/neurons_dotplot_TFs_fig_1F.eps", width = 24,height=100, units = "cm")

##Fig. 2
#Fig. 2A
mouse_lizard_CCA_integrated
Idents(mouse_lizard_CCA_integrated) <- mouse_lizard_CCA_integrated$species
png("./plots/mouse_lizard_CCA_integrated_DimPlot_species_fig_2A.png",height=1024,width=1024)
DimPlot(object = mouse_lizard_CCA_integrated, reduction ="umap",shuffle=T, cols=c(adjustcolor("#998ec3", alpha=0.4), adjustcolor("#f1a340", alpha=0.4)),label=F, pt.size=1) + NoLegend()
dev.off()
Idents(mouse_lizard_CCA_integrated) <- mouse_lizard_CCA_integrated$integrated_clusters
png("./plots/mouse_lizard_CCA_integrated_DimPlot_integrated-clusters_fig_2A.png",height=1024,width=1024)
DimPlot(object = mouse_lizard_CCA_integrated, reduction ="umap",label=F, pt.size=1) + NoLegend()
dev.off()
png(paste0("./plots/mouse_lizard_CCA_integrated_DimPlot_brain-divisions_fig_2A.png"),height=1024,width=1024)
DimPlot(object = mouse_lizard_CCA_integrated, reduction ="umap", group.by="int_developmental_compartment", cols=c("#FF64B0","#9ACF8D","#00C08B","#53aae5","#C77CFF"),label=F, pt.size=1) + NoLegend()
dev.off()
#Fig. 2B/SXX
integrated_genes_mus <- c("Tcf7l2","Rorb","Zic1","Tbr1","Foxp2","Gbx2","Meis2","Gng13","Mef2c")
for (feature in integrated_genes_mus)
{
  png(paste0("./plots/mouse_lizard_CCA_integrated_DimPlot_FeaturePlot_", feature,"_mus_no_legend.png"),height=1024,width=1024)
  print(FeaturePlot(object = mus, features = feature, pt.size=1) + NoLegend())
  dev.off()
}
integrated_genes_liz <- c("TCF7L2","RORB","ZIC1","TBR1","FOXP2","GBX2","MEIS2","GNG13","MEF2C")
for (feature in integrated_genes_liz)
{
  png(paste0("./plots/mouse_lizard_CCA_integrated_DimPlot_FeaturePlot_", feature,"_liz_no_legend.png"),height=1024,width=1024)
  print(FeaturePlot(object = liz, features = feature, pt.size=1) + NoLegend())
  dev.off()
}

##Fig. 3
#Fig. 3A
results.norm_reordered <- label_transfer_matrix_mus_reference[order(match(rownames(label_transfer_matrix_mus_reference), rev(liz_names_anatomical))), order(match(colnames(label_transfer_matrix_mus_reference), mus_names))]
pdf(file="./plots/label_transfer_matrix_mus_reference_re-ordered_anatomical_fig_3A.pdf", width=30, height=40)
corrplot(results.norm_reordered, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()
#filtering based on >25% matches (Fig. 3A)
results.norm_reordered_cropped <- results.norm_reordered[rowMaxs(results.norm_reordered) > 25, colMaxs(results.norm_reordered) > 25]
pdf(file="./plots/label_transfer_matrix_mus_reference_re-ordered_anatomical_compact-25_matches_fig_3A.pdf", width=20, height=35)
corrplot(results.norm_reordered_cropped, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()

#Fig. 3B
pdf("./plots/maximum_label_transfer_score_by_cluster_developmental_compartment.pdf")
ggplot(dev_comp_max_LT, aes(x = dev_comp_max_LT$region, y = dev_comp_max_LT$max_LT)) +
  geom_violin() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  xlab("Developmental Compartment") +
  ylab("maximum label transfer score by cluster [%]") +
  ggtitle("Maximum label transfer score for developmental compartments") +
  theme_classic()
dev.off()
ggsave("./plots/maximum_label_transfer_score_by_cluster_developmental_compartment.eps", width = 20,height=15, units = "cm")
#Fig. 3C see above --> 'Fig. 2B/SXX'

#Fig. 3D see label transfer analysis code

##Fig. 4
#Fig. 4A
png("./plots/INs_integrated_DimPlot_integrated_clusters_fig_4A.png",height=1024,width=1024)
DimPlot(object = INs_integrated, reduction ="umap", label=F, pt.size=2)
dev.off()
png("./plots/INs_integrated_DimPlot_mus_fig_4A.png",height=512,width=512)
DimPlot(object = INs_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#998ec3"),label=F, pt.size=2, order=c("mouse", "lizard"))+ NoLegend()
dev.off()
png("./plots/INs_integrated_DimPlot_liz_fig_4A.png",height=512,width=512)
DimPlot(object = INs_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#f1a340"),label=F, pt.size=2, order=c("lizard","mouse"))+ NoLegend()
dev.off()
#Fig. 4B
png("hyp_integrated_DimPlot_integrated_clusters_fig_4B.png",height=1024,width=1024)
DimPlot(object = hyp_integrated, reduction ="umap",label=F, pt.size=2) + NoLegend()
dev.off()
png("./plots/hyp_integrated_DimPlot_mus_fig_4B.png",height=512,width=512)
DimPlot(object = hyp_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#998ec3"),label=F, pt.size=2, order=c("mouse", "lizard"))+ NoLegend()
dev.off()
png("./plots/hyp_integrated_DimPlot_liz_fig_4B.png",height=512,width=512)
DimPlot(object = hyp_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#f1a340"),label=F, pt.size=2, order=c("lizard","mouse"))+ NoLegend()
dev.off()
#Fig. 4E
INs_integrated_red <- subset(INs_integrated, idents=c(6,8,9), invert=T)
levels(INs_integrated_red) <- rev(c(7,2,5,3,4,1))
genes <- c("ZEB2","LHX6","SATB1","MEF2C","SOX6","BCL11A","POU3F3","ZBTB16","PROX1","NR2F2","NFIB","NR3C2","AFF2","NR2F1","NFIX")
pdf("./plots/INs_integrated_conserved_TFs_fig_4E.pdf",width = 8)
DotPlot(INs_integrated_red, features = rev(genes), split.by="species",assay="SCT", cols=c("#998ec3","#f1a340"))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse + lizard integrated clusters")
dev.off()
ggsave("./plots/INs_integrated_red_colored_fig_4E.eps")
#Fig. 4C
png("./plots/SC_integrated_DimPlot_mus_fig_4C.png",height=512,width=512)
DimPlot(object = SC_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#998ec3"),label=F, pt.size=2, order=c("mouse", "lizard"))+ NoLegend()
dev.off()
png("./plots/SC_integrated_DimPlot_liz_fig_4C.png",height=512,width=512)
DimPlot(object = SC_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#f1a340"),label=F, pt.size=2, order=c("lizard","mouse"))+ NoLegend()
dev.off()
#Fig. 4F
hyp_integrated_red <- subset(hyp_integrated, idents=c(7,8), invert=T)
genes <- c("SIM1","FOSL2","TSHZ1","NR4A1","EBF3","ARX","ISL1","FEZF1","NR5A1","NR2F2")
levels(hyp_integrated_red) <- c(5,6,1,4,2,3)
pdf("./plots/hypothalamus_integrated_DotPlot_conserved_TFs_fig_4F.pdf",width = 8)
DotPlot(hyp_integrated_red, features = rev(genes), split.by="species",assay="SCT", cols=c("#998ec3","#f1a340"))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse + lizard integrated clusters")
dev.off()
ggsave("./plots/hypothalamus_integrated_DotPlot_conserved_TFs_fig_4F.eps")
#Fig. 4G
SC_integrated_red <- subset(SC_integrated, idents=8, invert=T)
levels(SC_integrated_red) <- rev(c(7,6,5,2,4,3,1,9))
genes <- c("LHX9","TCF7L2","MEIS2","ZFHX4","EBF3","OTX2","GATA3","NFIX","PAX7","SOX14","NFIB","ST18","IRX2","ZEB2")
pdf("/plots/SC_integrated_red_colored_fig_4G.pdf",width = 8)
DotPlot(SC_integrated_red, features = rev(genes), split.by="species",assay="SCT", cols=c("#998ec3","#f1a340"))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse + lizard integrated clusters")
dev.off()
ggsave("/plots/SC_integrated_red_colored_fig_4G.eps")
#Fig. 4D
png("thal_saunders_integrated_DimPlot_fig_4D.png",height=1024,width=1024)
DimPlot(object = thal_saunders_integrated, reduction ="umap", label=F, pt.size=2)+ NoLegend())
dev.off()
png("thal_saunders_integrated_DimPlot_mus_fig_4D.png",height=512,width=512)
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#998ec3"),label=F, pt.size=2, order=c("mouse", "lizard"))+ NoLegend())
dev.off()
png("thal_saunders_integrated_DimPlot_liz_fig_4D.png",height=512,width=512)
DimPlot(object = thal_saunders_integrated, reduction ="umap", group.by="species", cols=c("#afafaf","#f1a340"),label=F, pt.size=2, order=c("lizard","mouse"))+ NoLegend()
dev.off()
#Fig. 4H
thal_integrated_red <- subset(thal_integrated, idents=c(6,7), invert=T)
levels(thal_integrated_red) <- rev(c(3,5,4,2,1))
genes <- c("TCF7L2","ZIC1","FOXP2","ID4","NR2F2","MEIS2","SIX3","ISL1","ESRRG","ZEB2","NPAS3","LHX1")
pdf("./plots/thal_integrated_DotPlot_fig_4H.pdf",width = 8)
DotPlot(thal_integrated_red, features = rev(genes), split.by="species",assay="SCT", cols=c("#998ec3","#f1a340"))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse + lizard integrated clusters")
dev.off()
ggsave("./plots/thal_integrated_fig_4H.eps")

##Fig 5
#Fig 5B
png("./plots/thalamus_subclustered_DimPlot_fig_5B.png",height=1024,width=1024)
DimPlot(object = liz_thal,label=F, cols=custom_colors, pt.size=2)+ NoLegend()
dev.off()
thalamus_genes <- c("OPN4","CBLN1","LOC110073234")
for (feature in thalamus_genes)
{
  png(paste0("./plots/thalamus_FeaturePlot_", feature,"_fig_5B.png"),height=1024,width=1024)
  print(FeaturePlot(object = liz_thal, features = feature, pt.size=2.5))
  dev.off()
}
#Fig 5C
dmt_1 <- WhichCells(thal_glu, idents="DMT_1")
dmt_2 <- WhichCells(thal_glu, idents="DMT_2")
dmt_3 <- WhichCells(thal_glu, idents="DMT_3")
dmt_4 <- WhichCells(thal_glu, idents="DMT_4")
DMT <- list(dmt_1,dmt_2,dmt_3,dmt_4)
pdf("./plots/thal_glu_lizard-DMT_highlighted_fig_5C.pdf")
DimPlot(object = thal_glu, reduction ="pca", pt.size=0.5, cells.highlight=DMT, cols.highlight=rev(c("#00FDFE","#14DDD8","#1DB2AE","#2A9390")),sizes.highlight=0.5, na.value = "grey25",shuffle=T) + NoLegend()+ggtitle("lizard-thalamic-regions highlighted")
DimPlot(object = thal_glu, reduction ="pca", pt.size=0.5, cells.highlight=DMT, cols.highlight=rev(c("#00FDFE","#14DDD8","#1DB2AE","#2A9390")),sizes.highlight=0.5, na.value = "grey25") +ggtitle("lizard-thalamic-regions highlighted")
dev.off()
png("./plots/thal_glu_lizard-DMT_highlighted_fig_5C.png",height=1024,width=1024)
DimPlot(object = thal_glu, reduction ="pca", pt.size=2, cells.highlight=DMT, cols.highlight=rev(c("#00FDFE","#14DDD8","#1DB2AE","#2A9390")),sizes.highlight=2, na.value = "grey25",shuffle=T) + NoLegend()+ggtitle("lizard DMT highlighted")
dev.off()
#Fig 5D
pdf("./plots/thal_glu_PC_Heatmap_40_features_fig_5E.pdf")
DimHeatmap(thal_glu, dims = 1,nfeatures=40, balanced=T)
dev.off()

#Fig 5E
Idents(thal_integrated) <- thal_integrated$species
png("./plots/thal_integrated_DimPlot_species_fig_5EF.png",height=1024,width=1024)
DimPlot(object = thal_integrated, reduction ="umap",shuffle=T, cols=c(adjustcolor("#998ec3", alpha=0.4), adjustcolor("#f1a340", alpha=0.4)),label=F, pt.size=2) + NoLegend()
dev.off()
Idents(thal_integrated) <- thal_integrated$thalamus_subcluster
dmt_1 <- WhichCells(thal_integrated, idents="DMT_1")
dmt_2 <- WhichCells(thal_integrated, idents="DMT_2")
dmt_3 <- WhichCells(thal_integrated, idents="DMT_3")
dmt_4 <- WhichCells(thal_integrated, idents="DMT_4")
DMT <- c(dmt_1,dmt_2,dmt_3,dmt_4)
MT_DMT <- WhichCells(thal_integrated, idents="MT/DMT")
DLT <- WhichCells(thal_integrated, idents="DLT")
Rot_1 <- WhichCells(thal_integrated, idents="Rot_1")
Rot_2 <- WhichCells(thal_integrated, idents="Rot_2")
Rot_3 <-  WhichCells(thal_integrated, idents="Rot_3")
ROT <- c(Rot_1,Rot_2,Rot_3)
MT_1 <-  WhichCells(thal_integrated, idents="MT_1")
MT_2 <-  WhichCells(thal_integrated, idents="MT_2")
MT_3 <-  WhichCells(thal_integrated, idents="MT_3")
MT_4 <-  WhichCells(thal_integrated, idents="MT_4")
MT_5 <-  WhichCells(thal_integrated, idents="MT_5")
MT_6 <-  WhichCells(thal_integrated, idents="MT_6")
MT_7 <- WhichCells(thal_integrated, idents="MT_7")
MT <- c(MT_DMT,MT_1,MT_2,MT_3,MT_4,MT_5,MT_6,MT_7)
liz_thalamus <- list(MT,DLT,ROT,DMT)
pdf("./plots/thalamus_integrated_lizard-thalamic-regions_highlighted_fig_5E.pdf")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=liz_thalamus, cols.highlight=c("#00FDFE","#B56EB7","#B33BB7","#F192F4"),sizes.highlight=1.5, na.value = "grey25",shuffle=T) + NoLegend()+ggtitle("lizard-thalamic-regions highlighted")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=liz_thalamus, cols.highlight=c("#00FDFE","#B56EB7","#B33BB7","#F192F4"),sizes.highlight=1.5, na.value = "grey25") +ggtitle("lizard-thalamic-regions highlighted")
dev.off()
Idents(thal_integrated) <- thal_integrated$Projection
prefrontal <- WhichCells(thal_integrated, idents="Prefrontal")
auditory <- WhichCells(thal_integrated, idents="Auditory")
motor <- WhichCells(thal_integrated, idents="Motor")
soma <- WhichCells(thal_integrated, idents="Somatosensory")
visual <- WhichCells(thal_integrated, idents="Visual")
phil_lateral <- list(auditory,motor,soma,visual,prefrontal)
pdf("./plots/thal_integrated_phillips-projection_highlighted_fig_5E.pdf")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=phil_lateral, cols.highlight=c("#00FDFE","#FF00FF","#B33BB7","#B56EB7","#F192F4"),sizes.highlight=1.5, na.value = "grey25") + NoLegend()+ggtitle("Phillips2019 Projection")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=phil_lateral, cols.highlight=c("#00FDFE","#FF00FF","#B33BB7","#B56EB7","#F192F4"),sizes.highlight=1.5, na.value = "grey25") +ggtitle("Phillips2019 Projection")
dev.off()
#Fig 5F
Idents(thal_integrated) <- thal_integrated$ClusterID
DEINH1 <- WhichCells(thal_integrated, idents="DEINH1")
DEINH2 <- WhichCells(thal_integrated, idents="DEINH2")
zeisel_rtn <- list(DEINH1,DEINH2)
pdf("./plots/thal_integrated_zeisel-RTN_highlighted_fig_5F.pdf")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=zeisel_rtn, cols.highlight=rev(c("#F9FF00", "#e1e600")),sizes.highlight=1.5, na.value = "grey25") + NoLegend()+ggtitle("Zeisel2018")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=zeisel_rtn, cols.highlight=rev(c("#F9FF00", "#e1e600")),sizes.highlight=1.5, na.value = "grey25") +ggtitle("Zeisel2018")
dev.off()
Idents(thal_integrated) <- thal_integrated$subcluster
three_one <- WhichCells(thal_integrated, idents="3-1")
three_two <- WhichCells(thal_integrated, idents="3-2")
saunders_rtn <- list(three_one,three_two)
pdf("./plots/thal_integrated_saunders-RTN_highlighted_fig_5F.pdf")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=saunders_rtn, cols.highlight=rev(c("#F9FF00", "#e1e600")),sizes.highlight=1.5, na.value = "grey25") + NoLegend()+ggtitle("Saunders2018")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=saunders_rtn, cols.highlight=rev(c("#F9FF00", "#e1e600")),sizes.highlight=1.5, na.value = "grey25") +ggtitle("Saunders2018")
dev.off()
Idents(thal_integrated) <- thal_integrated$subcluster
lateral <- WhichCells(thal_integrated, idents=c("2-1","2-2","2-4","2-5","2-6","2-8","2-9","2-10","2-11"))
pdf("./plots/thal_integrated_saunders-lateral_highlighted_fig_5F.pdf")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=lateral, cols.highlight=rev(c("#F192F4")),sizes.highlight=1.5, na.value = "grey25") + NoLegend()+ggtitle("Saunders2018")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=lateral, cols.highlight=rev(c("#F192F4")),sizes.highlight=1.5, na.value = "grey25") +ggtitle("Saunders2018")
dev.off()
Idents(thal_integrated) <- thal_integrated$thalamus_subcluster
rtn_1 <- WhichCells(thal_integrated, idents="RTn_1")
rtn_2 <- WhichCells(thal_integrated, idents="RTn_2")
rtn_3 <- WhichCells(thal_integrated, idents="RTn_3")
rtn_4 <- WhichCells(thal_integrated, idents="RTn_4")
lizard_rtn <- list(rtn_1,rtn_2,rtn_3,rtn_4)
pdf("./plots/thal_integrated_lizard-RTN_highlighted_fig_5F.pdf")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=lizard_rtn, cols.highlight=rev(c("#F9FF00", "#e1e600","#c8cd00","#aeb300")),sizes.highlight=1.5, na.value = "grey25") + NoLegend()+ggtitle("Lizard")
DimPlot(object = thal_integrated, reduction ="umap", pt.size=0.5, cells.highlight=lizard_rtn, cols.highlight=rev(c("#F9FF00", "#e1e600","#c8cd00","#aeb300")),sizes.highlight=1.5, na.value = "grey25") +ggtitle("Lizard")
dev.off()
#Fig 5G
rtn_genes <- rev(unique(c("SLC32A1", "SLC6A1",  "MEIS2", "SIX3", "ISL1", "PAX6", "LOC110073234","NOS1","KCNJ3","P2RY1","CACNA1G","GSG1L2","SCUBE1","MAN2A1","ANOS1","RASD2","ST18","VIM","CHODL","CALB1","FAM19A1","PVALB","TMTC2","LOC110090477","GPR157","KANK4","LOC110091118","KCNAB3")))
RTN <- subset(liz_thal, idents=c("RTn_1","RTn_2","RTn_3","RTn_4"))
levels(RTN) <- c("RTn_1","RTn_2","RTn_3","RTn_4")
RTN <- SCTransform(RTN, vars.to.regress = c("animal","chemistry"), verbose = FALSE, return.only.var.genes = F)
pdf("./plots/RTN_DotPlot_fig_5G.pdf",width = 12,height=4)
DotPlot(RTN, features = rtn_genes) + theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()
ggsave("./plots/RTN_DotPlot_fig_5G.eps", width = 32,height=8, units = "cm")

####Supplemental figures
#Fig. S2B
cells_genes <- c("SNAP25","SLC17A6","SLC32A1","SOX4","SOX11","GFAP","DRAXIN","PDGFRA","GPR17","MAG","C1QC","SSPO","TTR","SOX18","ACTA2","COL1A1","CD24","MKI67","JCHAIN")
for (feature in cells_genes)
{
  png(paste0("./plots/cells_FeaturePlot_", feature,"_fig_S2B.png"),height=1024,width=1024)
  print(FeaturePlot(object = cells, features = feature))
  dev.off()
}

#Fig. S3B:
Idents(cells) <- cells$chemistry
pdf("./plots/cells_VlnPlot_split_10x-chemistry_fig_S3B.pdf")
VlnPlot(cells, features = c("nCount_RNA","nFeature_RNA"), pt.size=0)
dev.off()
Idents(neurons) <- neurons$chemistry
pdf("../plots/neurons_SCTransform_VlnPlot_split_10x-chemistry_fig_S3B.pdf")
VlnPlot(neurons_clean, features = c("nCount_RNA","nFeature_RNA"), pt.size=0)
dev.off()
Idents(neurons) <- neurons$cluster_annotation

#Fig. S3C
png("./plots/neurons_labeled_DimPlot_SCTransform_fig_S3C.png",height=1024,width=1024)
DimPlot(object = neurons, reduction ="umap",label=F, pt.size=1) + NoLegend()
dev.off()
region_genes <- c("GATA3","TCF7L2","OTX2","FOXG1","TAL1","ZIC1")
for (feature in region_genes)
{
  png(paste0("./plots/neurons_regions_FeaturePlot_", feature,"_fig_S3C.png"),height=1024,width=1024)
  print(FeaturePlot(object = neurons, features = feature, pt.size=1))
  dev.off()
}

#Fig. S4A

#Fig. SXX
rn <- as.data.frame(label_transfer_matrix_mus_reference)
rn$pairs <- "cluster pairs"
pdf("./plots/histogram_label-transfer_scores_log1p.pdf")
ggplot(rn, aes(x = Freq)) + geom_histogram(color="black", fill="white", binwidth=2) + scale_y_continuous(trans = scales::log1p_trans(), breaks=c(0,1,10,100,1000,10000,100000)) +
theme_bw() +
labs(y="Frequency (log1p)",
x="projection score [%]",
title="Histogram of projection scores")
dev.off()


#Fig. SI optic tectum
liz_SC_clusters <- c(paste0("MEGLUT",seq(1,13)),paste0("MEGABA",seq(8,15)),paste0("MEGABA",seq(19,34)))
liz_SC_cluster_order <- c("MEGLUT7", "MEGLUT6", "MEGLUT13", "MEGLUT12", "MEGLUT9", "MEGLUT8", "MEGLUT10", "MEGLUT11", "MEGLUT4", "MEGLUT3", "MEGLUT5", "MEGLUT2", "MEGLUT1", "MEGABA10", "MEGABA11", "MEGABA9", "MEGABA15", "MEGABA8", "MEGABA13", "MEGABA19", "MEGABA21", "MEGABA20", "MEGABA24", "MEGABA23", "MEGABA25", "MEGABA26", "MEGABA14", "MEGABA30", "MEGABA29", "MEGABA31", "MEGABA12", "MEGABA34", "MEGABA33", "MEGABA27", "MEGABA28", "MEGABA22", "MEGABA32")
liz_SC <- subset(neurons, idents = liz_SC_clusters)
liz_SC$data_source <- "Lizard"
liz_SC$species <- "lizard"
levels(liz_SC) <- rev(liz_SC_cluster_order)
genes <- c("SLC17A6", "SLC32A1", "TAC1", "CCK", "PENK", "DACH2", "MMEL1", " TAL1", "ANOS1", "COL19A1", "VIP")
pdf("./plots/liz_SC_neurons_DotPlot.pdf",width = 6, height=10)
DotPlot(liz_SC, features = rev(genes),assay="SCT")+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("lizard dotplot")
dev.off()
ggsave("./plots/liz_SC_neurons_DotPlot.eps", width = 15, height=20, units = "cm")


#Fig. SI Hypothalamus
genes <- c("SLC17A6", "SLC32A1", "OTP" , "LOC110091671", "AGRP", "POMC", "FOXB1", "LHR", "CRH", "NR5A1", "TRH", "SIM1", "ISL1", "PROK2", "HCRT", "FEZF1", "LHX8", "LHX1", "ARX", "SIX3")
pdf("./plots/liz_hyp_neurons_DotPlot.pdf",width = 8, heigh=20)
DotPlot(liz_hyp_neurons, features = rev(genes),assay="SCT")+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("lizard dotplot")
dev.off()
ggsave("./plots/liz_hyp_neurons_DotPlot.eps", width = 16,height=35, units = "cm")

#Fig SI Thalamus
thal_clusters_liz <- c(paste0("DMT_",seq(1:4)),"MT/DMT","DLT",paste0("Rot_",seq(1:3)),paste0("MT_",seq(1:7)),paste0("AT_",seq(1:3)),paste0("VT_",seq(1:4)),paste0("IGL_",seq(1:2)),"LGv",paste0("RTn_",seq(1:4)))
liz_thal <- subset(liz_thal, idents = thal_clusters_liz)
liz_thal <- SCTransform(liz_thal,vars.to.regress=c("animal","chemistry"))
levels(liz_thal) <- rev(thal_clusters_liz)
genes <- c("SLC17A6", "SLC32A1", "TCF7L2", "GBX2", "LHX2", "PROX1", "CBLN1", "CALB1", "PVALB", "OPN4", "VIP", "MEIS2", "PAX6", "ZIC1", "NPY", "LOC110073234")
pdf("./plots/liz_thal_neurons_DotPlot.pdf",width = 9, heigh=8)
DotPlot(liz_thal, features = rev(genes),assay="SCT")+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("lizard dotplot")
dev.off()
ggsave("./plots/liz_thal_neurons_DotPlot.eps", width = 22,height=19, units = "cm")

#Fig SI thalamus mouse integration
Idents(thal_integrated) <- thal_integrated$ClusterID
DEGLU3 <- WhichCells(thal_integrated, idents="DEGLU3")
Idents(thal_integrated) <- thal_integrated$Projection
prefrontal <- WhichCells(thal_integrated, idents="Prefrontal")
Idents(thal_integrated) <- thal_integrated$subcluster
two_two <- WhichCells(thal_integrated, idents="2-2")
two_three <- WhichCells(thal_integrated, idents="2-3")
two_seven <- WhichCells(thal_integrated, idents="2-7")
saunders_medial <- c(two_two,two_three,two_seven)
medial <- list(DEGLU3,prefrontal,saunders_medial)
pdf("./plots/mouse_glutamatergic_thalamus_medial-nuclei_highlighted.pdf")
DimPlot(object = thal_integrated, reduction ="pca", pt.size=0.5, cells.highlight=medial, cols.highlight=rev(c("#00FDFE","#14DDD8","#1DB2AE")),sizes.highlight=0.5, na.value = "grey25",shuffle=T) + NoLegend()+ggtitle("mouse-medial-thalamic-regions highlighted")
DimPlot(object = thal_integrated, reduction ="pca", pt.size=0.5, cells.highlight=medial, cols.highlight=rev(c("#00FDFE","#14DDD8","#1DB2AE")),sizes.highlight=0.5, na.value = "grey25") +ggtitle("mouse-medial-thalamic-regions highlighted")
dev.off()
png("./plots/mouse_glutamatergic_thalamus_medial-nuclei_highlighted.png",height=1024,width=1024)
DimPlot(object = thal_integrated, reduction ="pca", pt.size=2, cells.highlight=medial, cols.highlight=rev(c("#00FDFE","#14DDD8","#1DB2AE","#2A9390")),sizes.highlight=2, na.value = "grey25",shuffle=T) + NoLegend()+ggtitle("mouse-medial-thalamic-regions highlighted")
dev.off()


#Fig SI SAMap plotting
#integrated UMAPs
mus_liz <- merge(l6_r2_cns_neurons,neurons)
mus_liz[["umap"]]<- CreateDimReducObject(embeddings = bi_umap, key = "UMAP_")
mm_umap <- read.csv(file="../data/SAMap_UMAP_mm.csv", header=T, row.names=1)
pv_umap <- read.csv(file="../data/SAMap_UMAP_pv.csv", header=T, row.names=1)
umap <- as.matrix(rbind(mm_umap,pv_umap))
mus_liz[["SAMap_umap"]]<- CreateDimReducObject(embeddings = umap, key = "SAMap_UMAP_", assay = DefaultAssay(mus_liz))
png("./plots/SAMap_UMAP.png",height=2048,width=2048)
DimPlot(object = mus_liz, reduction ="SAMap_umap",label=F, pt.size=2) + NoLegend()
dev.off()
png("./plots/SAMap_UMAP_ClusterID.png",height=2048,width=2048)
DimPlot(object = mus_liz, reduction ="SAMap_umap",,label=T,repel=T,label.size=5, pt.size=2,group.by="ClusterID") + NoLegend()
dev.off()
png("./plots/SAMap_UMAP_cluster-annotation.png",height=2048,width=2048)
DimPlot(object = mus_liz, reduction ="SAMap_umap",label=T,repel=T,label.size=5, pt.size=2,group.by="cluster_annotation") + NoLegend()
dev.off()
png("./plots/SAMap_UMAP_species.png",height=2048,width=2048)
DimPlot(object = mus_liz, reduction ="SAMap_umap",label=T,repel=T,label.size=0.5, pt.size=2,group.by="species", cols=c("#998ec3","#f1a340"))
dev.off()
mus <- subset(mus_liz, subset = species == "mm")
liz <- subset(mus_liz, subset = species == "pv")
png("./plots/SAMap_UMAP_liz_only.png",height=1024,width=1024)
DimPlot(object = liz, reduction ="SAMap_umap", pt.size=1,group.by="species", cols=c("#f1a340")) + NoLegend()
dev.off()
png("./plots/SAMap_UMAP_mus_only.png",height=1024,width=1024)
DimPlot(object = mus, reduction ="SAMap_umap",pt.size=1,group.by="species", cols=c("#998ec3")) + NoLegend()
dev.off()
#SAMap alignment score matrix
SAMap_mtx <- read.csv(file="../data/SAMap_matrix_20220509.csv", header=T, row.names=1)
mus_names <- paste("mm",mus_names,sep="_")
liz_names <- paste("pv",liz_names,sep="_")
SAMap_mtx_t <- t(SAMap_mtx)
SAMap_mtx_t_reordered <- SAMap_mtx_t[order(match(rownames(SAMap_mtx_t), liz_names)), order(match(colnames(SAMap_mtx_t), mus_names))]
pdf(file="./plots/SAMap_results_matrix.pdf", width=30, height=40)
corrplot(SAMap_mtx_t_reordered, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()
#filtering based on >25% matches
SAMap_mtx_t_reordered_cropped <- SAMap_mtx_t_reordered[rowMaxs(SAMap_mtx_t_reordered) > 0.25, colMaxs(SAMap_mtx_t_reordered) > 0.25]
pdf(file="./plots/SAMap_results_matrix_compact-25-rev.pdf", width=20, height=35)
corrplot(SAMap_mtx_t_reordered_cropped, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()
dev_comp <- unique(neurons$developmental_compartment)
SAMap_mtx_t.mat <- as.matrix(SAMap_mtx_t)
results.max <- apply(SAMap_mtx_t.mat, 1, max)
tel <- subset(neurons, subset = developmental_compartment == "Telencephalon")
tel_clusters <- unique(tel$cluster_annotation)
tel_clusters <- paste("pv",tel_clusters,sep="_")
tel_LT <- as.data.frame(results.max[tel_clusters])
colnames(tel_LT) <- "max_LT"
tel_LT$region <- "Telencephalon"
die <- subset(neurons, subset = developmental_compartment == "Diencephalon")
die_clusters <- unique(die$cluster_annotation)
die_clusters <- paste("pv",die_clusters,sep="_")
die_LT <- as.data.frame(results.max[die_clusters])
colnames(die_LT) <- "max_LT"
die_LT$region <- "Diencephalon"
mes <- subset(neurons, subset = developmental_compartment == "Mesencephalon")
mes_clusters <- unique(mes$cluster_annotation)
mes_clusters <- paste("pv",mes_clusters,sep="_")
mes_LT <- as.data.frame(results.max[mes_clusters])
colnames(mes_LT) <- "max_LT"
mes_LT$region <- "Mesencephalon"
rho <- subset(neurons, subset = developmental_compartment == "Rhombencephalon")
rho_clusters <- unique(rho$cluster_annotation)
rho_clusters <- paste("pv",rho_clusters,sep="_")
rho_LT <- as.data.frame(results.max[rho_clusters])
colnames(rho_LT) <- "max_LT"
rho_LT$region <- "Rhombencephalon"
dev_comp_max_LT <- rbind(tel_LT,die_LT,mes_LT,rho_LT)
dev_comp_max_LT$region <- factor(dev_comp_max_LT$region,levels = unique(dev_comp_max_LT$region))
pdf("./plots/maximum_SAMap_score_by_cluster_brain_division.pdf")
ggplot(dev_comp_max_LT, aes(x = dev_comp_max_LT$region, y = dev_comp_max_LT$max_LT)) +
  geom_violin() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  xlab("Developmental Compartment") +
  ylab("maximum SAMap score by cluster [%]") +
  ggtitle("Maximum SAMap score for brain divisions") +
  theme_classic()
dev.off()
ggsave("./plots/maximum_SAMap_score_by_cluster_brain_division.eps", width = 20,height=15, units = "cm")
