library(future)
plan("multiprocess", workers = 12)
options(future.globals.maxSize= 62914560000) # 60GB (60000*1024^2)

library(Seurat)
library(dplyr)
library(cowplot)
library(zoo)
library(ggplot2)
library(loomR)
library(scater)
library(stringr)
library(corrplot)
library(matrixStats)

#reading objects for integration
neurons <- readRDS(file="../data/neurons_SCTransform_annotated.rds")
l6_r2_cns_neurons <- readRDS(file="../data/l6_r2_cns_neurons_Seurat.rds")
l6_r2_cns_neurons <- AddMetaData(l6_r2_cns_neurons, metadata = Idents(l6_r2_cns_neurons), col.name='ClusterID')


one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
colnames(one2one) <- c("pogona","orthology","mouse")
one2one$mouse <- toupper(one2one$mouse)
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)

one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

mus.raw <- GetAssayData(l6_r2_cns_neurons)
rownames(mus.raw) <- toupper(rownames(mus.raw))
mus.raw <- mus.raw[rownames(mus.raw) %in% one2one$mouse,]

one2one.mouse <- one2one[one2one$mouse %in% rownames(mus.raw),]
one2one.mouse <- one2one.mouse[order(match(one2one.mouse$mouse, rownames(mus.raw))),]
all.equal(one2one.mouse$mouse, rownames(mus.raw))
rownames(mus.raw) <- one2one.mouse$pogona
liz.raw <- GetAssayData(neurons)
liz.raw <- liz.raw[rownames(liz.raw) %in% one2one.mouse$pogona,]
common.genes <- intersect(rownames(mus.raw),rownames(liz.raw))
mus.raw <- mus.raw[rownames(mus.raw) %in% common.genes,]
liz.raw <- liz.raw[rownames(liz.raw) %in% common.genes,]

#integrated analysis
liz <- CreateSeuratObject(liz.raw)
mus <- CreateSeuratObject(mus.raw)
mus.metadata <- l6_r2_cns_neurons@meta.data
mus <- AddMetaData(mus, metadata = mus.metadata)
liz.metadata <- neurons@meta.data
liz <- AddMetaData(liz, metadata = liz.metadata)
liz$species <-"lizard"
mus$species <- "mouse"

nVarGenes <- 1500
nDims_integration_anchor <- 40

mus <- SCTransform(mus, return.only.var.genes = FALSE)
liz <- SCTransform(liz,vars.to.regress=c("animal","chemistry"), return.only.var.genes = FALSE)
DefaultAssay(mus) <- "SCT"
DefaultAssay(liz) <- "SCT"


########### Label Transfer: liz query, mouse reference
neuron_anchors <- FindTransferAnchors(reference = mus, query = liz, dims = 1:nDims_integration_anchor, reduction = "cca")
predicitons_cca <- TransferData(anchorset = neuron_anchors, refdata = mus$ClusterID, dims = 1:nDims_integration_anchor, weight.reduction = "cca")
liz <- AddMetaData(liz, metadata = predicitons_cca)

table(liz@meta.data$predicted.id)
results <- table(liz@meta.data$cluster_annotation, liz@meta.data$predicted.id)
# IF NO LIZARD CELL MAPS TO A CERTAIN MOUSE IDENTITY, THAT MOUSE IDENTITY WILL BE MISSING IN THE FINAL HEATMAP!!!
results.norm <- 100*(results/rowSums(results))

mus.ids <- colnames(results.norm)
results.norm <- results.norm[,mus.ids]

saveRDS(results.norm, file=paste0("../data/liz_query_mouse_reference_normalized_ClusterID_vargenes_",nVarGenes,"_dims_",nDims_integration_anchor,".rds"))

#label_transfer_matrix_mus_reference <- readRDS(file="../data/liz_query_mouse_reference_normalized_ClusterID_vargenes_1500_dims_40.rds")

pdf(file=paste0("../plotting/plots/liz_query_mouse_reference_normalized_ClusterID_vargenes_",nVarGenes,"_dims",nDims_integration_anchor,".pdf", sep=""), width=30, height=40)
#hist(liz@meta.data$prediction.score.max, breaks=20)
corrplot(results.norm, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()

###re-odering based on anatomical position (Fig3 A)
mus_names <- c(paste0("OBDOP", seq(from=1, to=2)), paste0("OBINH", seq(from=1, to=5)), paste0("OBNBL", seq(from=1, to=5)), paste0("TEGLU", seq(from=1, to=24)), "CR", paste0("DGGRC", seq(from=1, to=2)), paste0("DGNBL", seq(from=1, to=2)), paste0("TEINH", seq(from=1, to=21)), paste0("MSN", seq(from=1, to=6)), "SZNBL", "SEPNBL", paste0("DEGLU", seq(from=1, to=5)),"DETPH", "DECHO2", paste0("HYPEP", seq(from=1, to=8)), paste0("DEINH", seq(from=1, to=8)), paste0("MEGLU", seq(from=1, to=14)), paste0("MEINH", seq(from=1, to=14)), "CBGRC", paste0("CBINH", seq(from=1, to=2)), "CBPC", paste0("CBNBL", seq(from=1, to=2)), paste0("HBGLU", seq(from=1, to=10)), paste0("HBINH", seq(from=1, to=9)), paste0("SCGLU", seq(from=1, to=10)), paste0("SCINH", seq(from=1, to=11)), "TECHO", "DECHO1", "MBCHO1",paste0("HBCHO", seq(from=1, to=4)), paste0("MBDOP", seq(from=1, to=2)), paste0("HBSER", seq(from=1, to=5)), "HBADR", "HBNOR")

liz_names_anatomical <- c(paste0("TEGLUT", seq(from=1, to=29)),paste0("TEGABA", seq(from=1, to=24)),paste0("DIGLUT", seq(from=1, to=55)),paste0("DIGABAGLUT", seq(from=1, to=8)),paste0("DICHOL", seq(from=1, to=3)),paste0("DISECR", seq(from=1, to=7)),paste0("DIGABA", seq(from=1, to=34)),paste0("MEGLUT", seq(from=1, to=13)),paste0("MEGABA", seq(from=1, to=45)),paste0("CBGLUT", seq(from=1, to=5)),paste0("CBGABA", seq(from=1, to=2)),"RHGABA1","TECHOL1",paste0("DIDOPA", seq(from=1, to=2)),paste0("DISERT", seq(from=1, to=2)),"MESERT1","RHCHOL1")

liz_names_anatomical_rev <- rev(liz_names_anatomical)

results.norm_reordered <- results.norm[order(match(rownames(results.norm), liz_names_anatomical_rev)), order(match(colnames(results.norm), mus_names_mus_ref))]
pdf(file=paste0("../plotting/plots/liz_query_mouse_reference_normalized_ClusterID_vargenes_",nVarGenes,"_dims",nDims_integration_anchor,"_PCA_re-ordered_anatomical-rev.pdf", sep=""), width=30, height=40)
corrplot(results.norm_reordered, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()

#filtering based on >25% matches (Fig3 A)
results.norm_reordered_cropped <- results.norm_reordered[rowMaxs(results.norm_reordered) > 25, colMaxs(results.norm_reordered) > 25]
pdf(file=paste0("../plotting/plots/liz_query_mouse_reference_normalized_ClusterID_vargenes_",nVarGenes,"_dims",nDims_integration_anchor,"_reordered_dendrogram_names_compact-25-rev.pdf", sep=""), width=20, height=35)
corrplot(results.norm_reordered_cropped, order="original",tl.pos="lt", method="color", tl.col="black", is.corr=F)
dev.off()

########### liz query, mouse reference
r_mrlq <- results.norm
df_mrlq <- as.data.frame(r_mrlq)
df_mrlq <- df_mrlq[order(df_mrlq$Freq, decreasing=T),]
names(df_mrlq)[1] <- "lizard_cluster"
names(df_mrlq)[2] <- "mouse_cluster"
names(df_mrlq)[3] <- "overlap"
df_mrlq$ranked_order <- 1:dim(df_mrlq)[1]
df_mrlq$cluster_combinations <- ""
for (i in 1:dim(df_mrlq)[1]){
  df_mrlq$cluster_combinations[i] <- paste0(df_mrlq$mouse_cluster[i],"-", df_mrlq$lizard_cluster[i])
}
write.csv(df_mrlq, "../data/mouse_reference_lizard_query_ordered_list.csv")

#maximum value by developmental compartment
dev_comp <- unique(neurons$developmental_compartment)
results.norm.mat <- as.matrix(results.norm)
results.max <- apply(results.norm.mat, 1, max)

tel <- subset(neurons, subset = developmental_compartment == "Telencephalon")
tel_clusters <- unique(tel$cluster_annotation)
tel_LT <- as.data.frame(results.max[tel_clusters])
colnames(tel_LT) <- "max_LT"
tel_LT$region <- "Telencephalon"

die <- subset(neurons, subset = developmental_compartment == "Diencephalon")
die_clusters <- unique(die$cluster_annotation)
die_LT <- as.data.frame(results.max[die_clusters])
colnames(die_LT) <- "max_LT"
die_LT$region <- "Diencephalon"

mes <- subset(neurons, subset = developmental_compartment == "Mesencephalon")
mes_clusters <- unique(mes$cluster_annotation)
mes_LT <- as.data.frame(results.max[mes_clusters])
colnames(mes_LT) <- "max_LT"
mes_LT$region <- "Mesencephalon"

rho <- subset(neurons, subset = developmental_compartment == "Rhombencephalon")
rho_clusters <- unique(rho$cluster_annotation)
rho_LT <- as.data.frame(results.max[rho_clusters])
colnames(rho_LT) <- "max_LT"
rho_LT$region <- "Rhombencephalon"

dev_comp_max_LT <- rbind(tel_LT,die_LT,mes_LT,rho_LT)
dev_comp_max_LT$region <- factor(dev_comp_max_LT$region,levels = unique(dev_comp_max_LT$region))

saveRDS(dev_comp_max_LT,file="../data/maximum_label_transfer_score_by_cluster_developmental_compartment.rds")


pdf("../plotting/plots/maximum_label_transfer_score_by_cluster_developmental_compartment.pdf")
ggplot(dev_comp_max_LT, aes(x = dev_comp_max_LT$region, y = dev_comp_max_LT$max_LT)) +
  geom_violin() +
  geom_jitter(shape=16, position=position_jitter(0.2)) +
  xlab("Developmental Compartment") +
  ylab("maximum label transfer score by cluster [%]") +
  ggtitle("Maximum label transfer score for developmental compartments") +
  theme_classic()
dev.off()
ggsave("maximum_label_transfer_score_by_cluster_developmental_compartment.eps", width = 20,height=15, units = "cm")


sel_cluster_pairs <- c(1,2,5,9,13,15,17,18,24,26,54,86)

#lizard and mouse specific markers (only 1-to-1 orthologs) calculated in the single-species datasets
Idents(liz) <- liz$cluster_annotation
Idents(mus) <- mus$ClusterID

for (i in sel_cluster_pairs){
  liz_cluster <- as.character(df_mrlq$lizard_cluster[i])
  mus_cluster <- as.character(df_mrlq$mouse_cluster[i])
  liz_markers <- FindMarkers(object = liz, ident.1=liz_cluster, only.pos = T, assay="SCT")
  mus_markers <- FindMarkers(object = mus, ident.1=mus_cluster, only.pos = T, assay="SCT")
  saveRDS(liz_markers, file=paste0("../data/"_,i,"_",df_mrlq$lizard_cluster[i],"_markers_liz.rds"))
  saveRDS(mus_markers, file=paste0("../data/"_,i,"_",df_mrlq$mouse_cluster[i],"_markers_mus.rds"))
  write.csv(liz_markers, file=paste0("../data/"_,i,"_",df_mrlq$lizard_cluster[i],"_markers_liz.csv"))
  write.csv(mus_markers, file=paste0("../data/"_,i,"_",df_mrlq$mouse_cluster[i],"_markers_mus.csv"))
}

#orthologous genes, function to convert from/to lizard/mouse gene names
one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
colnames(one2one) <- c("pogona","orthology","mouse")
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)
#removing duplicates
one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

mouse_to_lizard_gene_names <- function(mouse_gene_names) {
  lizard_gene_names <- c()
  for (i in 1:length(mouse_gene_names)){
    o2o_gene <- one2one[one2one$mouse==mouse_gene_names[i],]
    gene <- o2o_gene$pogona
    lizard_gene_names <- c(lizard_gene_names,gene)
  }
  return(lizard_gene_names)
}

lizard_to_mouse_gene_names <- function(lizard_gene_names) {
  mouse_gene_names <- c()
  for (i in 1:length(lizard_gene_names)){
    o2o_gene <- one2one[one2one$pogona==lizard_gene_names[i],]
    gene <- o2o_gene$mouse
    mouse_gene_names <- c(mouse_gene_names,gene)
  }
  return(mouse_gene_names)
}

#cleaning up by excluding noisy genes
noisy.mus <- scan(file="../data/noisy_genes_mouse.txt", what=character())
noisy.mus <- mouse_to_lizard_gene_names(noisy.mus)
noisy.liz <- scan(file="../data/noisy_genes_lizard.txt", what=character())
noisy.liz <- mouse_to_lizard_gene_names(noisy.liz)
noisy <- intersect(noisy.mus,noisy.liz)

neurons_markers <- readRDS(file="../data/neurons_SCTransform_annotated_markers_re-ordered.rds")

l6_r2_cns_neurons <- SCTransform(l6_r2_cns_neurons, return.only.var.genes = FALSE)

l6_r2_cns_neurons_markers <- FindAllMarkers(object = l6_r2_cns_neurons, only.pos = TRUE, min.pct = 0.25, assay='SCT')
saveRDS(l6_r2_cns_neurons_markers, file="../data/l6_r2_cns_neurons_Seurat_markers.rds")
l6_r2_cns_neurons_markers_top25 <- l6_r2_cns_neurons_markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(l6_r2_cns_neurons_markers_top25, file = "../data/l6_r2_cns_neurons_Seurat_markers_top25.csv")

#selected cluster pairs
liz_clusters_ordered <- c("TEGLUT8","TEGLUT25","TEGABA21","DICHOL1","DIGLUT20","DIGABA32","MEGABA10","MEGLUT7","MEGLUT9","CBGLUT5","CBGABA1","CBGABA2")
mus_clusters_ordered <- c("TEGLU8","TEGLU24","TEINH2","DECHO2","DEGLU3","DEINH1","MEINH10","MEGLU5","MEGLU4","CBGRC","CBPC","CBINH1")

neurons_markers_clusters <- neurons_markers[neurons_markers$cluster %in% liz_clusters_ordered,]
l6_r2_cns_neurons_markers_clusters <- l6_r2_cns_neurons_markers[l6_r2_cns_neurons_markers$cluster %in% mus_clusters_ordered,]


#subsetting lizard+mouse markers to o2o orthologous genes
neurons_markers_o2o <- neurons_markers[neurons_markers$gene %in% one2one$pogona,]
l6_r2_cns_neurons_markers_o2o <- l6_r2_cns_neurons_markers[l6_r2_cns_neurons_markers$gene %in% one2one$mouse,]

common_markers_conc <- matrix(,0,7)
lizard_markers_conc <- matrix(,0,7)
mouse_markers_conc <- matrix(,0,7)

for (i in c(1:12)){
  mus_clu <- mus_clusters_ordered[i]
  liz_clu <- liz_clusters_ordered[i]
  neurons_markers_clusters <- neurons_markers_o2o[neurons_markers_o2o$cluster %in% liz_clu,]
  l6_r2_cns_neurons_markers_clusters <- l6_r2_cns_neurons_markers_o2o[l6_r2_cns_neurons_markers_o2o$cluster %in% mus_clu,]
  mus_markers <- mouse_to_lizard_gene_names(l6_r2_cns_neurons_markers_clusters$gene)
  liz_markers <- neurons_markers_clusters$gene
  #common marker genes
  common_marker_genes <- intersect(mus_markers,liz_markers)
  common_marker_genes_mouse <- lizard_to_mouse_gene_names(common_marker_genes)


  common_markers <- neurons_markers_clusters[neurons_markers_clusters$gene %in% common_marker_genes,]
  common_markers_conc <- rbind(common_markers_conc, common_markers)

  #species specific markers
  neurons_markers_clusters_specific <- neurons_markers_clusters[!neurons_markers_clusters$gene %in% common_marker_genes,]
  lizard_markers_conc <- rbind(lizard_markers_conc, neurons_markers_clusters_specific)
  l6_r2_cns_neurons_markers_clusters_specific <- l6_r2_cns_neurons_markers_clusters[!l6_r2_cns_neurons_markers_clusters$gene %in% common_marker_genes_mouse,]
  mouse_markers_conc <- rbind(mouse_markers_conc, l6_r2_cns_neurons_markers_clusters_specific)
}

###check if all markers are in the SCT assay
mus_SCT_genes <- rownames(l6_r2_cns_neurons[["SCT"]]) #gives list of all genes scaled
mus_SCT_genes <- mouse_to_lizard_gene_names(mus_SCT_genes)
liz_SCT_genes <- rownames(neurons[["SCT"]]) #gives list of all genes scaled
liz_mus_SCT_genes_liz <- intersect(mus_SCT_genes,liz_SCT_genes)
liz_mus_SCT_genes_mus <- lizard_to_mouse_gene_names(liz_mus_SCT_genes_liz)

lizard_markers_conc_cropped <- lizard_markers_conc[lizard_markers_conc$gene %in% liz_mus_SCT_genes_liz,]
mouse_markers_conc_cropped <- mouse_markers_conc[mouse_markers_conc$gene %in% liz_mus_SCT_genes_mus,]
common_markers_conc_cropped <- common_markers_conc[common_markers_conc$gene %in% liz_mus_SCT_genes_liz,]

lizard_markers_conc_top10 <- lizard_markers_conc_cropped %>% group_by(cluster) %>% top_n(10, avg_logFC)
lizard_markers_conc_top10$mouse_gene <- lizard_to_mouse_gene_names(lizard_markers_conc_top10$gene)
mouse_markers_conc_top10 <- mouse_markers_conc_cropped %>% group_by(cluster) %>% top_n(10, avg_logFC)
mouse_markers_conc_top10$lizard_gene <- mouse_to_lizard_gene_names(mouse_markers_conc_top10$gene)
#common_markers_conc_top10 <- common_markers_conc_cropped %>% group_by(cluster) %>% top_n(10, avg_logFC)
common_markers_conc_cropped$mouse_gene <- ""
common_markers_conc_cropped$mouse_gene <- lizard_to_mouse_gene_names(common_markers_conc_cropped$gene)

write.csv(lizard_markers_conc_cropped, file = "../data/lizard_markers_conc_wo_non_SCT.csv")
write.csv(mouse_markers_conc_cropped, file = "../data/mouse_markers_conc_wo_non_SCT.csv")
write.csv(lizard_markers_conc_top10, file = "../data/lizard_markers_conc_top10_wo_non_SCT.csv")
write.csv(mouse_markers_conc_top10, file = "../data/mouse_markers_conc_top10_wo_non_SCT.csv")
write.csv(common_markers_conc, file = "../data/common_markers_conc_wo_non_SCT.csv")
cm <- common_markers_conc
cm$marker_type <- "common markers (statistics shown only for lizard cluster expression)"
mm <- mouse_markers_conc_cropped
mm$marker_type <- "mouse"
lm <- lizard_markers_conc_cropped
lm$marker_type <- "lizard"

markers_combined <- rbind(cm, lm, mm)
write.csv(markers_combined, file = "../data/label_transfer_cluster_pairs_markers_combined.csv")

liz_pairs <- subset(neurons, idents=liz_clusters_ordered)
levels(liz_pairs) <- rev(liz_clusters_ordered)
mouse_pairs <- subset(l6_r2_cns_neurons, idents=mus_clusters_ordered)
levels(mouse_pairs) <- rev(mus_clusters_ordered)

liz_genes <- c(common_markers_conc_cropped$gene,lizard_markers_conc_top10$gene,mouse_markers_conc_top10$lizard_gene)
mouse_genes <- c(common_markers_conc_cropped$mouse_gene,lizard_markers_conc_top10$mouse_gene,mouse_markers_conc_top10$gene)


pdf("../plotting/plots/liz_heatmap_conserved_liz_mus_markers_wo_non_SCT.pdf")
DoHeatmap(liz_pairs, features=liz_genes) + NoLegend()
dev.off()

pdf("../plotting/plots/mouse_heatmap_conserved_liz_mus_markers_wo_non_SCT.pdf")
DoHeatmap(mouse_pairs, features=mouse_genes) + NoLegend()
dev.off()

pdf("../plotting/plots/liz_heatmap_conserved_liz_mus_markers_wo_non_SCT_legend.pdf")
DoHeatmap(liz_pairs, features=liz_genes)
dev.off()

pdf("../plotting/plots/mouse_heatmap_conserved_liz_mus_markers_wo_non_SCT_legend.pdf")
DoHeatmap(mouse_pairs, features=mouse_genes)
dev.off()
