# Pogona_scRNA-seq

Analysis pipeline for the data scRNA-seq data from _Pogona vitticeps_ brain reported in: 

**'Molecular diversity and evolution of neuron types in the amniote brain'** by David Hain, Tatiana Gallego-Flores, Michaela Klinkmann, Angeles Macias, Elena Ciirdaeva, Anja Arends, Christina Thum, Georgi Tushev, Friedrich Kretschmer, Maria Antonietta Tosches and Gilles Laurent


## Contents


* **./analysis/cell analysis.R**

Analysis and clustering of all cells from Cellranger output, including filtering by SVM. Produces 'Pogona_vitticeps_cells.rds'.

* **neurons_subclustering_analysis.R**

Extraction and subclustering of neuronal data. Produces the annotated neuronal object 'neurons_SCTransform_annotated.rds'.

* **neurons_hierarchical_clustering.R**

Hierarchical clustering of neuronal data.

* **cumulative_gene_family_expression_by_cluster.R**

Code for the cumulative cluster expression.

* **mouse-lizard_Seurat-CCA-integration.R**

CCA-based whole brain integration of neuronal _Pogona vitticeps_ data with neuronal mouse data from Zeisel et al. 2018. Also includes GO-analysis of conserved marker genes for the whole-brain integrated clusters and riverplot.


* **label_transfer_integration.R**

Code for label transfer based integration of mouse and _Pogona_ data and the HeatMaps for highly conserved cluster pairs.

* **hypothalamus_integration.R**, **optic-tectum_superior-colliculus_integration.R**, **interneuron_integration_and_subclustering.R**, **thalamus_saunders_integration.R**

Code for the region-specific integrations with mouse data shown in Fig.4.

* **thalamus_subclustering.R**

Code for the thalamic subclustering.

* **thalamus_integration_saunders_phillips_zeisel_lizard.R**

Code for the thalamic integration with mouse data from Zeisel et al. 2018, Saunders et al. 2018 and Phillips et al. 2019.

* **thalamus_PCA_analysis.R**

Code for the PCA analysis of _Pogona_ and mouse glutamatergic clusters. Including GO-analysis.

* **Pogona_mouse_SAMap_integration.ipynb**

Code for the SAMap based integration of mouse and _Pogona_ data.



## Notes
The scripts assume that the data resides in ../data/, this includes cellranger outputs for the sequencing data. 

For the comparative analyses the following data has to be downloaded first:

Zeisel et al. 2018: [l6_r2_cns_neurons](https://storage.googleapis.com/linnarsson-lab-loom/l6_r2_cns_neurons.loom)

Saunders et al. 2018: [F_GRCm38.81.P60Thalamus](http://dropviz.org/)

Phillips et al. 2019: [GSE133912](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE133912)

Romanov et al. 2017: [GSE74672](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE74672)

Xie et al. 2021: [GSE162404](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE162404) (meta data was obtained after personal communication with the authors)



