library(future)
plan("multiprocess", workers = 12)
options(future.globals.maxSize= 62914560000) # 60GB (60000*1024^2)

library(Seurat)
library(dplyr)
library(ggplot2)
library(loomR)
library(gprofiler2)

#converting loom files to Seurat object
l6_r2_cns_neurons <- connect(filename = "../data/l6_r2_cns_neurons.loom", mode = "r")
l6_r2_cns_neurons_Seurat  <- as.Seurat(l6_r2_cns_neurons)
saveRDS(l6_r2_cns_neurons_Seurat, file="../data/l6_r2_cns_neurons_Seurat.rds")
l6_r2_cns_neurons$close_all()

#reading in mouse and lizard data
l6_r2_cns_neurons <- readRDS(file="../data/l6_r2_cns_neurons_Seurat.rds")
l6_r2_cns_neurons <- AddMetaData(l6_r2_cns_neurons, metadata = Idents(l6_r2_cns_neurons), col.name='ClusterID')
Idents(l6_r2_cns_neurons) <- l6_r2_cns_neurons$ClusterID

neurons <- readRDS(file="../data/neurons_SCTransform_annotated.rds")
neurons$cluster_annotation <- Idents(neurons)

#reading and formatting o2o orthologs table
one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
colnames(one2one) <- c("pogona","orthology","mouse")
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)
#removing duplicates
one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

#extracting gene matrices from Seurat objects
mus.raw <- GetAssayData(l6_r2_cns_neurons)
liz.raw <- GetAssayData(neurons)

#subsetting lizard+mouse count matrices to o2o orthologous genes
liz.raw <- liz.raw[rownames(liz.raw) %in% one2one$pogona,]
mus.raw <- mus.raw[rownames(mus.raw) %in% one2one$mouse,]

#creating o2o list with expressed genes and re-ordering
one2one.liz <- one2one[one2one$pogona %in% rownames(liz.raw),]
one2one.liz <- one2one.liz[order(match(one2one.liz$pogona, rownames(liz.raw))),]
all.equal(one2one.liz$pogona, rownames(liz.raw))
# assigning mouse gene names to pogona genes
rownames(liz.raw) <- one2one.liz$mouse
#taking only o2o orthologs for mouse and lizard
common.genes <- intersect(rownames(liz.raw),rownames(mus.raw))
mus.raw <- mus.raw[rownames(mus.raw) %in% common.genes,]
liz.raw <- liz.raw[rownames(liz.raw) %in% common.genes,]

saveRDS(common.genes, "../data/zeisel-lizard_CCA_common_genes.rds")
write(common.genes, file="../data/zeisel-lizard_CCA_common_genes.txt")

dim(mus.raw)
dim(liz.raw)

#integrated analysis
liz <- CreateSeuratObject(liz.raw)
mus <- CreateSeuratObject(mus.raw)

mus.metadata <- l6_r2_cns_neurons@meta.data
mus <- AddMetaData(mus, metadata = mus.metadata)

liz.metadata <- neurons@meta.data
liz <- AddMetaData(liz, metadata = liz.metadata)

liz$species <-"lizard"
mus$species <- "mouse"
#only take '10X v3 chemistry' data. Previous integrations showed strong effects of v2 chemistry on integration.
liz <- subset(liz, subset = chemistry == "v3")

Idents(liz) <- liz$cluster_annotation
Idents(mus) <- mus$ClusterID

noisy.mus <- NoisyGenes(mus, 0.2, levels(Idents(mus)))
write(noisy.mus, file="../data/noisy_genes_mouse.txt")

noisy.liz <- NoisyGenes(liz, 0.2, levels(Idents(liz)))
write(noisy.liz, file="../data/noisy_genes_v3-only_lizard.txt")
noisy_intersect <- intersect(noisy.mus,noisy.liz)

#populate RNA assay in case it's needed later
mus <- NormalizeData(mus)
mus <- ScaleData(mus, vars.to.regress=c("nCount_RNA","nFeature_RNA"), features=rownames(mus))
liz <- NormalizeData(liz)
liz <- ScaleData(liz, vars.to.regress=c("nCount_RNA", "nFeature_RNA","animal"), features=rownames(liz))
#use SCT assay for integration
mus <- SCTransform(mus, , return.only.var.genes = FALSE)
liz <- SCTransform(liz,vars.to.regress=c("animal"), return.only.var.genes = FALSE)
DefaultAssay(mus) <- "SCT"
DefaultAssay(liz) <- "SCT"

brain.list <- list(mus,liz)

nVarGenes <- 1500
nDims_CCA <- 40

int_features <- SelectIntegrationFeatures(object.list = brain.list, nfeatures = nVarGenes)
int_features_clean <- int_features[!int_features %in% noisy_intersect]

brain.list <- PrepSCTIntegration(object.list = brain.list, anchor.features = int_features_clean)

brain_anchors <- FindIntegrationAnchors(object.list = brain.list, anchor.features= int_features_clean, normalization.method = "SCT", dims = 1:nDims_CCA, reduction="cca")
brain_integrated <- IntegrateData(anchorset = brain_anchors, normalization.method = "SCT", dims = 1:nDims_CCA)

brain_integrated <- RunPCA(brain_integrated, npcs = 200, verbose = FALSE)
pdf(paste0("../plotting/plots/brain_integrated_Elbowplot_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.pdf"))
print(ElbowPlot(object = brain_integrated, ndims = 200))
dev.off()
nDims_PCA <- 50

brain_integrated <- RunUMAP(brain_integrated, reduction = "pca", dims = 1:nDims_PCA)

png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_region_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="region", label=T, repel=T, pt.size=0.01))
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_species_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="species", label=T, repel=T, pt.size=0.01))
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_mus_ClusterID_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="ClusterID", label=T, repel=T, pt.size=0.01) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_TaxonomyRank4_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="TaxonomyRank4", label=T, repel=T, pt.size=0.01))
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_developmental_compartment_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="Developmental_compartment", label=T, repel=T, pt.size=0.01))
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_liz_cluster_annotation_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T, pt.size=0.01) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_Region_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="Region", label=T, repel=T, pt.size=0.01) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_labeled_DimPlot_liz_chemistry_UMAP_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="chemistry", label=T, repel=T, pt.size=0.01) + NoLegend())
dev.off()

brain_integrated <- FindNeighbors(object = brain_integrated, dims=1:nDims_PCA)

res <- 0.2

brain_integrated <- FindClusters(object = brain_integrated, resolution=res)
png(paste0("../plotting/plots/brain_integrated_UMAP_nVarGenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA_clustered_res", res,"_no_legend.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", label=T, repel=T, pt.size=0.01) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_UMAP_nVarGenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA_clustered_res", res,".png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", label=T, repel=T, pt.size=0.01))
dev.off()

brain_integrated$integrated_clusters <- as.character(Idents(brain_integrated))


### subclustering of clusters 0 and 1
cl_0 <- subset(brain_integrated, idents=0)
cl_1 <- subset(brain_integrated, idents=1)

cl_0_1 <- subset(brain_integrated, idents=c(0,1))
cl_0_1.list <- SplitObject(object=cl_0_1, split.by="species")
cl_0_1.list[[1]] <- SCTransform(cl_0_1.list[[1]], return.only.var.genes = FALSE)
cl_0_1.list[[2]] <- SCTransform(cl_0_1.list[[2]], return.only.var.genes = FALSE, vars.to.regress=c("animal"))


nVarGenes <- 1500
nDims_CCA <- 40


int_features <- SelectIntegrationFeatures(object.list =cl_0_1.list, nfeatures = nVarGenes)
cl_0_1.list <- PrepSCTIntegration(object.list = cl_0_1.list, anchor.features = int_features)



cl_0_1_anchors <- FindIntegrationAnchors(object.list = cl_0_1.list, anchor.features= int_features, normalization.method = "SCT", dims = 1:nDims_CCA, reduction="cca")
cl_0_1_integrated <- IntegrateData(anchorset = cl_0_1_anchors, normalization.method = "SCT", dims = 1:nDims_CCA)

cl_0_1_integrated <- RunPCA(cl_0_1_integrated, npcs = 200, verbose = FALSE)
pdf(paste0("../plotting/plots/clusters_0_1_Elbowplot_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.pdf"))
print(ElbowPlot(object = cl_0_1_integrated, ndims = 200))
dev.off()
nDims_PCA <- 30

cl_0_1_integrated <- RunUMAP(cl_0_1_integrated, reduction = "pca", dims = 1:nDims_PCA)

cl_0_1_integrated <- FindNeighbors(object = cl_0_1_integrated, dims=1:nDims_PCA)
cl_0_1_integrated <- FindClusters(object = cl_0_1_integrated, resolution=0.1)
png(paste0("../plotting/plots/clusters_0_1_UMAP_nVarGenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA_clustered_res", cluster_res,".png"),height=1024,width=1024)
DimPlot(object = cl_0_1_integrated, reduction ="umap", label=T, repel=T, pt.size=0.1)
dev.off()


cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '0'="1-1")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '1'="0-1")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '2'="1-2")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '3'="0-2")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '4'="0-3")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '5'="0-4")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '6'="0-5")
cl_0_1_integrated <- RenameIdents(cl_0_1_integrated, '7'="0-6")
cl_0_1_integrated$integrated_clusters <- Idents(cl_0_1_integrated)

clusters_0_1_cells <- WhichCells(cl_0_1_integrated)

Idents(brain_integrated, cells=clusters_0_1_cells) <- Idents(cl_0_1_integrated)

png(paste0("../plotting/plots/brain_integrated_UMAP_0-1_reclustered.png"),height=1024,width=1024)
print(DimPlot(object = brain_integrated, reduction ="umap", label=T, repel=T, pt.size=0.1))
dev.off()

brain_integrated <- RenameIdents(brain_integrated, '0-1'='1')
brain_integrated <- RenameIdents(brain_integrated, '0-2'='2')
brain_integrated <- RenameIdents(brain_integrated, '0-3'='3')
brain_integrated <- RenameIdents(brain_integrated, '0-4'='4')
brain_integrated <- RenameIdents(brain_integrated, '0-5'='5')
brain_integrated <- RenameIdents(brain_integrated, '0-6'='6')
brain_integrated <- RenameIdents(brain_integrated, '1-1'='7')
brain_integrated <- RenameIdents(brain_integrated, '1-2'='8')
brain_integrated <- RenameIdents(brain_integrated, '2'='9')
brain_integrated <- RenameIdents(brain_integrated, '3'='10')
brain_integrated <- RenameIdents(brain_integrated, '4'='11')
brain_integrated <- RenameIdents(brain_integrated, '5'='12')
brain_integrated <- RenameIdents(brain_integrated, '5'='13')
brain_integrated <- RenameIdents(brain_integrated, '6'='14')
brain_integrated <- RenameIdents(brain_integrated, '7'='15')
brain_integrated <- RenameIdents(brain_integrated, '8'='16')
brain_integrated <- RenameIdents(brain_integrated, '9'='17')
brain_integrated <- RenameIdents(brain_integrated, '10'='18')
brain_integrated <- RenameIdents(brain_integrated, '11'='19')
brain_integrated <- RenameIdents(brain_integrated, '12'='20')
brain_integrated <- RenameIdents(brain_integrated, '13'='21')
brain_integrated <- RenameIdents(brain_integrated, '14'='22')
brain_integrated <- RenameIdents(brain_integrated, '15'='23')
brain_integrated <- RenameIdents(brain_integrated, '16'='24')
brain_integrated <- RenameIdents(brain_integrated, '17'='25')
brain_integrated <- RenameIdents(brain_integrated, '18'='26')
brain_integrated <- RenameIdents(brain_integrated, '19'='27')
brain_integrated <- RenameIdents(brain_integrated, '20'='28')
brain_integrated <- RenameIdents(brain_integrated, '21'='29')
brain_integrated <- RenameIdents(brain_integrated, '22'='30')
brain_integrated <- RenameIdents(brain_integrated, '23'='31')
brain_integrated <- RenameIdents(brain_integrated, '24'='32')
brain_integrated <- RenameIdents(brain_integrated, '25'='33')

brain_integrated$integrated_clusters <- Idents(brain_integrated)

png(paste0("../plotting/plots/brain_integrated_annotated_CCA__labeled_DimPlot_region_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="region", label=T, repel=T, pt.size=0.1))
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_species_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=1024,width=1024)
print(DimPlot(object = clusters_0_1, reduction ="umap", group.by="species", label=T, repel=T, pt.size=0.1))
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_mus_ClusterID_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="ClusterID", label=T, repel=T, pt.size=0.1) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_TaxonomyRank4_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="TaxonomyRank4", label=T, repel=T, pt.size=0.1))
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_developmental_compartment_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="Developmental_compartment", label=T, repel=T, pt.size=0.1))
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_liz_cluster_annotation_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T, pt.size=0.1) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_Region_UMAP_vargenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="Region", label=T, repel=T, pt.size=0.1) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_liz_chemistry_UMAP_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=2048,width=2048)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="chemistry", label=T, repel=T, pt.size=0.1) + NoLegend())
dev.off()
png(paste0("../plotting/plots/brain_integrated_annotated_CCA__DimPlot_integrated_clusters_UMAP_",nVarGenes,"_dims_",nDims_CCA,"_CCA.png"),height=1024,width=1024)
print(DimPlot(object = brain_integrated, reduction ="umap", group.by="integrated_clusters", label=T, repel=T, pt.size=0.1) + NoLegend())
dev.off()

brain_integrated$int_developmental_compartment <- ""
cell_idx <- colnames(brain_integrated)
for (i in 1:length(cell_idx)){
	if(brain_integrated$species[i] == "mouse"){
		brain_integrated$int_developmental_compartment[i] <- brain_integrated$Developmental_compartment[i]
	} else if(brain_integrated$species[i] == "lizard"){
		brain_integrated$int_developmental_compartment[i] <- brain_integrated$developmental_compartment[i]
	}
}

saveRDS(brain_integrated, file ="../data/brain_integrated_annotated_CCA_clustered_and_subclustered.rds")

#### conserved markers; w/o logfc
brain_integrated_markers_conc <- matrix(,0,14)
cluster_list <- c(1:3,5,7:14,16,17,20,22:25,31)

for (clu in cluster_list){
  brain_integrated_markers <- FindConservedMarkers(brain_integrated, ident.1=clu, grouping.var = "species", verbose = FALSE, assay="SCT", only.pos= TRUE)
  brain_integrated_markers$cluster <- ""
  saveRDS(brain_integrated_markers, file = paste0("../data/brain_integrated_conserved_markers_cluster_",clu,"_SCT_only_pos.rds"))
  write.csv(brain_integrated_markers, file = paste0("../data/brain_integrated_conserved_markers_cluster_",clu,"_SCT_only_pos.csv"))
  brain_integrated_markers$cluster <- clu
  brain_integrated_markers$gene <- rownames(brain_integrated_markers)
  brain_integrated_markers_conc <- rbind(brain_integrated_markers_conc, brain_integrated_markers)
}
write.csv(brain_integrated_markers_conc, file = paste0("../data/Data_S4_lizard-mouse_conserved_markers_table.csv"))


bi_unique_conserved_markers <- unique(brain_integrated_markers_conc$gene )
write(bi_unique_conserved_markers, file="../data/brain_integrated_unique_conserved_marker_genes_SCT_assay.txt")


noisy.mus <- toupper(noisy.mus)
noisy.liz <- toupper(noisy.liz)
noisy.geneset <- intersect(noisy.mus,noisy.liz)

universe <- common.genes
diffexpr <- bi_unique_conserved_markers
universe <- toupper(universe)
diffexpr <- toupper(diffexpr)

universe <- universe[!universe %in% noisy.geneset]
diffexpr <- diffexpr[!diffexpr %in% noisy.geneset]

gostres <- gost(query = diffexpr,
                organism = "mmusculus", ordered_query = FALSE,
                multi_query = FALSE, significant = TRUE, exclude_iea = FALSE,
                measure_underrepresentation = FALSE, evcodes = FALSE,
                user_threshold = 0.01, correction_method = "g_SCS",
                domain_scope = "annotated", custom_bg = universe,
                numeric_ns = "",  as_short_link = FALSE,sources = c("GO:MF", "GO:BP","GO:CC"))

gostplot(gostres, capped = FALSE, interactive = TRUE)

result_df <- gostres$result
result_df$GO_term <- paste(result_df$term_id,result_df$term_name)

result_df_top10 <- result_df %>% group_by(source) %>% top_n(10, -p_value)
result_df_top10$GO_term <- factor(result_df_top10$GO_term, levels = result_df_top10$GO_term[order(-log10(result_df_top10$p_value))])
pdf("../plotting/plots/conserved_marker_genes_GO-analysis.pdf")
ggplot(result_df_top10, aes(x=GO_term, y=-log10(p_value), fill=source)) +     geom_bar(stat="identity")  + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()
pdf("../plotting/plots/conserved_marker_genes_GO-analysis_bw.pdf")
ggplot(result_df_top10, aes(x=GO_term, y=-log10(p_value), fill=source)) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()
result_mf_top10 <- result_df_top10[result_df_top10$source=="GO:MF",]
result_cc_top10 <- result_df_top10[result_df_top10$source=="GO:CC",]
result_bp_top10 <- result_df_top10[result_df_top10$source=="GO:BP",]
pdf("../plotting/plots/GO-analysis_whole-brain_conserved_markers_bw_GO-MF.pdf")
ggplot(result_mf_top10, aes(x=GO_term, y=-log10(p_value))) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()
pdf("../plotting/plots/GO-analysis_whole-brain_conserved_markers_bw_GO-CC.pdf")
ggplot(result_cc_top10, aes(x=GO_term, y=-log10(p_value))) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()
pdf("../plotting/plots/GO-analysis_whole-brain_conserved_markers_bw_GO-BP.pdf")
ggplot(result_bp_top10, aes(x=GO_term, y=-log10(p_value))) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()


#re-annotating Zeisel et al. 2018 clusters
linnarsson_region_annotation <- read.csv(file = "linnarsson_region_annotation.csv")
brain_integrated$new_mouse_region <- NA
for (i in 1:length(linnarsson_region_annotation$Mouse.cluster.name)){
  print(i)
  mus_cluster <- as.character(linnarsson_region_annotation$Mouse.cluster.name[i])
  mus_region <- as.character(linnarsson_region_annotation$Region[i])
  cell_idx <- which(brain_integrated$ClusterID == mus_cluster)
  for (k in 1:length(cell_idx)){
    idx <- cell_idx[k]
    brain_integrated$new_mouse_region[idx] <- mus_region
    print(paste0("cluster: ",mus_cluster," cell: ", idx," renamed to ", mus_region, "; k ",k," of ", length(cell_idx)))
  }
}

#split lizard annotations further
cell_idx <- which(brain_integrated$assigned_region == "Optic Tectum")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	if(brain_integrated$neurotransmitter[idx]=="glutamate"){
		brain_integrated$assigned_region[idx] <- "Optic Tectum glutamate"
		print(paste0("found glutamatergic OT cell at ", i))
	} else if(brain_integrated$neurotransmitter[idx]=="GABA"){
		brain_integrated$assigned_region[idx] <- "Optic Tectum GABA"
		print(paste0("found GABAergic OT cell at ", idx))
	} else{
		print(paste0("no match to GABA/glut at ", idx))
	}

}

cell_idx <- which(brain_integrated$assigned_region == "Subpallium")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	if(brain_integrated$cluster_annotation[idx] %in% c("TEGABA16","TEGABA17","TEGABA18","TEGABA19","TEGABA20")){
		brain_integrated$assigned_region[idx] <- "LGE"
		print(paste0("found Striatum cell at ", i))
	} else if(brain_integrated$cluster_annotation[idx] %in% c("TEGABA21","TEGABA22","TEGABA23","TEGABA24")){
		brain_integrated$assigned_region[idx] <- "Septum"
		print(paste0("annotated Septum in cluster ", brain_integrated$cluster_annotation[idx]))
  } else if(brain_integrated$cluster_annotation[idx] %in% c(paste0("TEGABA", seq(from=1, to=15)))){
    brain_integrated$assigned_region[idx] <- "MGE"
    print(paste0("annotated interneurons in cluster ", brain_integrated$cluster_annotation[idx]))
	} else{
		print(paste0("nothing changed at ", brain_integrated$cluster_annotation[idx]))
	}
}

cell_idx <- which(brain_integrated$assigned_region == "Tegmentum")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	brain_integrated$assigned_region[idx] <- "Midbrain"
  print(paste0("changed to midbrain at ", brain_integrated$cluster_annotation[idx]))

}

cell_idx <- which(brain_integrated$assigned_region == "Diencephalon")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	brain_integrated$assigned_region[idx] <- "Diencephalon/Serotonergic"
  print(paste0("changed to serotonergic at ", brain_integrated$cluster_annotation[idx]))
}

cell_idx <- which(brain_integrated$assigned_region == "Other pallium")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	if(brain_integrated$cluster_annotation[idx] %in% c("TEGLUT15","TEGLUT16","TEGLUT17","TEGLUT18")){
		brain_integrated$assigned_region[idx] <- "Dentate gyrus"
		print(paste0("found Dentate gyrus cell at ", i))
	} else if(brain_integrated$cluster_annotation[idx] %in% c(paste0("TEGLUT", seq(from=19, to=26)))){
		brain_integrated$assigned_region[idx] <- "Other hippocampus"
		print(paste0("annotated Hippocampus in cluster ", brain_integrated$cluster_annotation[idx]))
	} else{
		print(paste0("nothing changed at ", brain_integrated$cluster_annotation[idx]))
	}
}

cell_idx <- which(brain_integrated$assigned_region == "Thalamus")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	if(brain_integrated$cluster_annotation[idx] %in% c(paste0("DIGABA", seq(from=28, to=34)))){
		brain_integrated$assigned_region[idx] <- "Prethalamus"
		print(paste0("found Thalamus GABA cell at ", i))
	} else if(brain_integrated$cluster_annotation[idx] %in% c(paste0("DIGLUT", seq(from=12, to=23)))){
		brain_integrated$assigned_region[idx] <- "Thalamus glutamate"
		print(paste0("annotated Thalamus glutamate in cluster ", brain_integrated$cluster_annotation[idx]))
	} else{
		print(paste0("nothing changed at ", brain_integrated$cluster_annotation[idx]))
	}
}

cell_idx <- which(brain_integrated$assigned_region == "Hypothalamus")
for (i in 1:length(cell_idx)){
	idx <- cell_idx[i]
	if(brain_integrated$cluster_annotation[idx] %in% c(paste0("DIGABAGLUT", seq(from=1, to=8)))){
		brain_integrated$assigned_region[idx] <- "Hypothalamus GABA/glutamate"
		print(paste0("found Hypothalamus  GABA/glutamate cell at ", i))
	} else if(brain_integrated$cluster_annotation[idx] %in% c(paste0("DIGABA", seq(from=4, to=27)))){
		brain_integrated$assigned_region[idx] <- "Hypothalamus GABA"
		print(paste0("annotated Hypothalamus GABA in cluster ", brain_integrated$cluster_annotation[idx]))
  } else if(brain_integrated$cluster_annotation[idx] %in% c(paste0("DIGLUT", seq(from=1, to=11)),paste0("DIGLUT", seq(from=24, to=28)),paste0("DIGLUT", seq(from=32, to=35)),paste0("DIGLUT", seq(from=44, to=55)))){
    brain_integrated$assigned_region[idx] <- "Hypothalamus glutamate"
    print(paste0("annotated Hypothalamus glutamate in cluster ", brain_integrated$cluster_annotation[idx]))
  } else if(brain_integrated$cluster_annotation[idx] %in% c(paste0("DISECR", seq(from=1, to=7)),paste0("DISERT", seq(from=1, to=2)),paste0("DIDOPA", seq(from=1, to=2)))){
    brain_integrated$assigned_region[idx] <- "Hypothalamus secretory/monoaminergic"
    print(paste0("annotated Hypothalamus secretory in cluster ", brain_integrated$cluster_annotation[idx]))
	} else{
		print(paste0("something strange at ", brain_integrated$cluster_annotation[idx]))
	}
}

#remove cluster 29 for riverplot as only cells from one species present
bi_subs <- subset(brain_integrated, idents = 29, invert=T)


mouse_regions <- table(bi_subs$integrated_clusters, bi_subs$new_mouse_region)
mouse_regions <- as.data.frame(mouse_regions)
mouse_regions$species <- "mouse"
colnames(mouse_regions) <- c("integrated_clusters","region","frequency","species")
lizard_regions <- table(bi_subs$integrated_clusters, bi_subs$assigned_region)
lizard_regions <- as.data.frame(lizard_regions)
lizard_regions$species <- "lizard"
colnames(lizard_regions) <- c("integrated_clusters","region","frequency","species")
alluvial_table <- rbind(lizard_regions,mouse_regions)
mouse_regions$mouse_regions <- mouse_regions$region
mouse_regions$lizard_regions <- NA
lizard_regions$lizard_regions <- lizard_regions$region
lizard_regions$mouse_regions <- NA
alluvial_table_species <- rbind(lizard_regions,mouse_regions)
write.csv(alluvial_table_species, file = "../data/brain_integrated_CCA_alluvial_table.csv")


alluvial_table_species$target_idx <- as.numeric(match(alluvial_table_species$integrated_clusters, names))-1
liz_source <- as.numeric(match(alluvial_table_species$lizard_regions, names))-1
liz_source <- liz_source[!is.na(liz_source)]
mus_source <- as.numeric(match(alluvial_table_species$mouse_regions, names))-1
mus_source <- mus_source[!is.na(mus_source)]

value <- as.numeric(alluvial_table_species$frequency)
target <- as.numeric(match(alluvial_table_species$integrated_clusters, names))-1
target_cropped <- target[1:length(liz_source)]
target_cropped2 <- target[c(length(liz_source)+1):length(target)]
source_new <-  as.numeric(c(liz_source, target_cropped2))
target_new <-  as.numeric(c(target_cropped, mus_source))
data <- data.frame(value = value,target=target_new,source=source_new)
data_filtered <- data[data$value > 0,]
clusters <- list(names = data.frame(names),data=data_filtered)
saveRDS(clusters, "../data/clusters_network3D_mouse_regions.rds")
#use R Desktop:
library(networkD3)
clusters <- readRDS("../data/clusters_network3D_mouse_regions.rds")
sankeyNetwork(Links = clusters$data, Nodes = clusters$names, Source = "source", Target = "target", Value = "value", NodeID = "names", units = "cells", fontSize = 8, nodeWidth = 40, nodePadding = 10, height = 800, width = 800, iterations = 10000, sinksRight = F)
