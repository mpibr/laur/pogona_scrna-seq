library(future)
plan("multiprocess", workers = 12)
options(future.globals.maxSize= 62914560000) # 60GB (60000*1024^2)

library(Seurat)
library(dplyr)
library(corrplot)
library(zoo)
library(ggplot2)


thal_saunders <- readRDS("../data/Thalamus_saunders.rds")
thal_saunders <- subset(thal_saunders, subset = nFeature_RNA > 1500)
thal_saunders$data_source <- "Saunders"
thal_saunders$species <- "mouse"

liz_thal <- readRDS("../data/thalamus_subclustered.rds")
liz_thal$data_source <- "Lizard"
liz_thal$species <- "lizard"
thal_clusters_liz <- c(paste0("DMT_",seq(1:4)),"MT/DMT","DLT",paste0("Rot_",seq(1:3)),paste0("MT_",seq(1:7)),paste0("AT_",seq(1:3)),paste0("VT_",seq(1:4)),paste0("IGL_",seq(1:2)),"LGv",paste0("RTn_",seq(1:4)))
liz_thal <- subset(liz_thal, idents = thal_clusters_liz)


one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
#formatting gene orthology matrix
colnames(one2one) <- c("pogona","orthology","mouse")
one2one$mouse <- toupper(one2one$mouse)
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)
one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

#extracting gene matrices
#Saunders
saunders.raw <- GetAssayData(thal_saunders)
rownames(saunders.raw) <- toupper(rownames(saunders.raw))
saunders.raw <- saunders.raw[rownames(saunders.raw) %in% one2one$mouse,]

#subsetting gene matrices to one-to-one orthologs, re-ordering
#Saunders
one2one.saunders <- one2one[one2one$mouse %in% rownames(saunders.raw),]
one2one.saunders <- one2one.saunders[order(match(one2one.saunders$mouse, rownames(saunders.raw))),]
all.equal(one2one.saunders$mouse, rownames(saunders.raw))

# assigning pogona gene names to mouse genes
rownames(saunders.raw) <- one2one.saunders$pogona


liz.raw <- GetAssayData(liz_thal)
liz.raw <- liz.raw[rownames(liz.raw) %in% one2one.saunders$pogona,]


common.genes <- intersect(rownames(saunders.raw),rownames(liz.raw))

length(common.genes)
#[1] 10396


saunders.raw <- saunders.raw[rownames(saunders.raw) %in% common.genes,]
liz.raw <- liz.raw[rownames(liz.raw) %in% common.genes,]

#integrated analysis
saunders <- CreateSeuratObject(saunders.raw)
liz <- CreateSeuratObject(liz.raw)

saunders.metadata <- thal_saunders@meta.data
saunders <- AddMetaData(saunders, metadata = saunders.metadata)

liz.metadata <- liz_thal@meta.data
liz <- AddMetaData(liz, metadata = liz.metadata)

saunders <- SCTransform(saunders)
liz <- SCTransform(liz,vars.to.regress=c("animal","chemistry"))

thal.list <- list(saunders,liz)

nfeatures <- 2000
nDims_CCA <- 25

features <- SelectIntegrationFeatures(object.list = thal.list, nfeatures = nfeatures)
thal.list <- PrepSCTIntegration(object.list = thal.list, anchor.features = features)


thal_anchors <- FindIntegrationAnchors(object.list = thal.list, anchor.features= features, normalization.method = "SCT", dims = 1:nDims_CCA, reduction="cca")
thal_integrated <- IntegrateData(anchorset = thal_anchors, normalization.method = "SCT", dims = 1:nDims_CCA)

thal_integrated <- RunPCA(thal_integrated, npcs = 100, verbose = FALSE)
pdf("../plotting/plots/thal_integrated_Elbowplot_",nfeatures,".pdf")
ElbowPlot(object = thal_integrated, ndims = 100)
dev.off()

nDims_PCA <- 25

thal_integrated <- RunUMAP(thal_integrated, reduction = "pca", dims = 1:nDims_PCA)
thal_integrated <- FindNeighbors(object = thal_integrated, dims=1:nDims_PCA)

pdf("../plotting/plots/thal_integrated_DimPlots_UMAP_",nfeatures,"_features_",nDims_PCA,"_PCs.pdf")
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="data_source"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="species"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T)+ggtitle("lizard cluster"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="thalamus_subcluster", label=T, repel=T)+ggtitle("lizard thalamus subcluster"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="subcluster", label=T, repel=T)+ggtitle("Saunders2018 cluster"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="chemistry"))
dev.off()



thal_integrated <- FindClusters(object = thal_integrated, resolution=0.2)

thal_integrated_mus <- subset(thal_integrated, subset = species == "mouse")
thal_integrated_liz <- subset(thal_integrated, subset = species == "lizard")

Idents(thal_integrated_mus) <- thal_integrated_mus$integrated_snn_res.0.2
Idents(thal_integrated_liz) <- thal_integrated_liz$integrated_snn_res.0.2

thal_integrated_mus <- SCTransform(thal_integrated_mus)
thal_integrated_liz <- SCTransform(thal_integrated_liz,vars.to.regress=c("animal","chemistry"))

thal_integrated_mus.markers <- FindAllMarkers(thal_integrated_mus, only.pos = T, min.pct = 0.25, logfc.threshold = 0.25, assay='SCT')
saveRDS(thal_integrated_mus.markers, "../data/thal_saunders_integrated_mus.markers.rds")
thal_integrated_mus.markers_top25 <- thal_integrated_mus.markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(thal_integrated_mus.markers_top25, file = "../data/thal_saunders_integrated_mus.markers_top25.csv")

thal_integrated_liz.markers <- FindAllMarkers(thal_integrated_liz, only.pos = T, min.pct = 0.25, logfc.threshold = 0.25, assay='SCT')
saveRDS(thal_integrated_liz.markers, "../data/thal_saunders_integrated_liz.markers.rds")
thal_integrated_liz.markers_top25 <- thal_integrated_liz.markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(thal_integrated_liz.markers_top25, file = "thal_saunders_integrated_liz.markers_top25.csv")


dat <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat) <- c("cluster","conserved_marker_gene")
dat_conc <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat_conc) <- c("cluster","conserved_marker_gene")

for (i in c(0:4)){
dat <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat) <- c("cluster","conserved_marker_gene")
cluster_markers_mus <- subset(thal_integrated_mus.markers, subset = cluster == i)
cluster_markers_liz <- subset(thal_integrated_liz.markers, subset = cluster == i)
common_genes <- intersect(cluster_markers_mus$gene,cluster_markers_liz$gene)
dat[1:length(common_genes),1] <- i
dat[,2] <- common_genes
dat_conc <- rbind(dat_conc, dat)

}

write.csv(dat_conc, file = "../data/thal_saunders_integrated_conserved_markers_by_clusters.csv")

TFs <- scan(file = '../data/211101_TFs_mouse_pogona.txt', what=character())
conserved_TF_TH <- intersect(TFs,dat_conc$conserved_marker_gene)
dat_conc_TFs <- dat_conc[dat_conc$conserved_marker_gene %in% TFs,]
dat_conc_TFs$cluster_incr <- c(dat_conc_TFs$cluster+1)
pdf("../plotting/plots/thalamus_conserved_TFs_by_cluster_histogram.pdf")
hist(dat_conc_TFs$cluster_incr, breaks=c(0.5,1.5,2.5,3.5,4.5,5.5),col="grey",
main="Conserved TFs by cluster",
xlab="cluster",
ylab="# of conserved TFs",
ylim=c(0,12))
dev.off()


thal_integrated$incr_clusters <- c(as.numeric(as.character(thal_integrated$integrated_snn_res.0.2))+1)
Idents(thal_integrated) <- thal_integrated$incr_clusters

png(paste0("thal_integrated_DimPlot_integrated_clusters.png"),height=1024,width=1024)
print(DimPlot(object = thal_integrated, reduction ="umap", label=F, pt.size=2))
dev.off()

saveRDS(thal_integrated, "../data/thal_saunders_integrated_clustered.rds")
