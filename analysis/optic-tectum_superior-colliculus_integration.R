library(future)
plan("multiprocess", workers = 12)
options(future.globals.maxSize= 62914560000) # 60GB (60000*1024^2)

library(Seurat)
library(dplyr)
library(corrplot)
library(zoo)
library(ggplot2)


setwd("/gpfs/laur/experiments/scRNA-seq/pogona_scrna-seq/comparative_analyses/OT-SC_comparison/20210927_only_Xie-liz_improved/")

xie_raw <- readRDS("../data/Xie_expression_matrix.rds")
xie_metadata <- read.csv(file = '/gpfs/laur/experiments/scRNA-seq/pogona_scrna-seq/Seurat_objects/Xie2021/metadata_Fig1A.csv', stringsAsFactors = FALSE, row.names=1)
xie_SC <- CreateSeuratObject(counts = xie_raw)
xie_SC <- subset(xie_SC, cells=rownames(xie_metadata))
xie_SC <- AddMetaData(xie_SC, metadata=xie_metadata, col.name='xie_clusters')
Idents(xie_SC) <- xie_SC$xie_clusters
xie_neurons <- c(paste0("Ex-",seq(1:9)),paste0("In-",seq(1:10)))
xie_SC <- subset(xie_SC, idents=xie_neurons)
xie_SC$data_source <- "Xie2021"
xie_SC$species <- "mouse"
xie_SC.metadata <- xie_SC@meta.data



neurons <- readRDS(file="../data/neurons_SCTransform_annotated.rds")
neurons$cluster_annotation <- Idents(neurons)
liz_SC_clusters <- c(paste0("MEGLUT",seq(1,13)),paste0("MEGABA",seq(8,15)),paste0("MEGABA",seq(19,34)))
liz_SC <- subset(neurons, idents = liz_SC_clusters)
liz_SC$data_source <- "Lizard"
liz_SC$species <- "lizard"
liz_SC.metadata <- liz_SC@meta.data


one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
#formatting gene orthology matrix
colnames(one2one) <- c("pogona","orthology","mouse")
one2one$mouse <- toupper(one2one$mouse)
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)
one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

#extracting gene matrices
#xie_SC
xie_SC.raw <- GetAssayData(xie_SC)
rownames(xie_SC.raw) <- toupper(rownames(xie_SC.raw))
xie_SC.raw <- xie_SC.raw[rownames(xie_SC.raw) %in% one2one$mouse,]

#subsetting gene matrices to one-to-one orthologs, re-ordering
#xie_SC
one2one.xie_SC <- one2one[one2one$mouse %in% rownames(xie_SC.raw),]
one2one.xie_SC <- one2one.xie_SC[order(match(one2one.xie_SC$mouse, rownames(xie_SC.raw))),]
all.equal(one2one.xie_SC$mouse, rownames(xie_SC.raw))

# assigning pogona gene names to mouse genes
rownames(xie_SC.raw) <- one2one.xie_SC$pogona

liz.raw <- GetAssayData(liz_SC)
liz.raw <- liz.raw[rownames(liz.raw) %in% one2one.xie_SC$pogona,]


common.genes <- intersect(rownames(xie_SC.raw),rownames(liz.raw))
length(common.genes)


xie_SC.raw <- xie_SC.raw[rownames(xie_SC.raw) %in% common.genes,]
liz.raw <- liz.raw[rownames(liz.raw) %in% common.genes,]

#integrated analysis
xie_SC <- CreateSeuratObject(xie_SC.raw)
liz_SC <- CreateSeuratObject(liz.raw)

xie_SC <- AddMetaData(xie_SC, metadata = xie_SC.metadata)
liz_SC <- AddMetaData(liz_SC, metadata = liz_SC.metadata)

xie_SC <- SCTransform(xie_SC)
liz_SC <- SCTransform(liz_SC,vars.to.regress=c("animal","chemistry"))


nVarGenes <- 1500
nDims_CCA <- 20
nDims_PCA <- 30


SC.list <- list(xie_SC,liz_SC)

int_features <- SelectIntegrationFeatures(object.list = SC.list, nfeatures = nVarGenes)
SC.list.loop <- PrepSCTIntegration(object.list = SC.list, anchor.features = int_features)


SC_anchors <- FindIntegrationAnchors(object.list = SC.list.loop, anchor.features= int_features, normalization.method = "SCT", dims = 1:nDims_CCA, reduction="cca")
SC_integrated <- IntegrateData(anchorset = SC_anchors, normalization.method = "SCT", dims = 1:nDims_CCA)


SC_integrated <- RunPCA(SC_integrated, npcs = 100, verbose = FALSE)
pdf(paste0("../plotting/plots/SC_integrated_Elbowplot_", nVarGenes,"VG_", nDims_CCA,"_CCs.pdf"))
print(ElbowPlot(object = SC_integrated, ndims = 100))
dev.off()
SC_integrated <- RunUMAP(SC_integrated, reduction = "pca", dims = 1:nDims_PCA)

pdf(paste0("../plotting/plots/SC_integrated_DimPlots_UMAP_", nVarGenes,"VG_", nDims_CCA,"_CCs_",nDims_PCA,"_PCs_SCTransform.pdf"))
#print(DimPlot(object = INs_integrated, reduction ="umap", label=T, repel=T)+ NoLegend())
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="species"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="data_source"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T)+ NoLegend() +ggtitle("lizard cluster"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="xie_clusters", label=T, repel=T)+ NoLegend()+ggtitle("Xie2021 cluster"))
dev.off()

SC_integrated <- FindNeighbors(object = SC_integrated, dims=1:nDims_PCA)
cluster_res <- c(0.2)
SC_integrated <- FindClusters(object = SC_integrated, resolution=cluster_res)



png(paste0("../plotting/plots/SC_integrated_species_UMAP_shuffled_transparent_pt_2.png"),height=1024,width=1024)
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="species", cols=c(adjustcolor("#998ec3", alpha=0.5),adjustcolor("#f1a340", alpha=0.5)),label=F, pt.size=2, order=c("lizard","mouse")))
dev.off()



png(paste0("../plotting/plots/SC_integrated_UMAP_nVarGenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA_clustered_res_", cluster_res,".png"),height=1024,width=1024)
DimPlot(object = SC_integrated, reduction ="umap", label=F, repel=T, pt.size=2) + NoLegend()
dev.off()
png(paste0("../plotting/plots/SC_integrated_UMAP_nVarGenes_",nVarGenes,"_dims_",nDims_CCA,"_CCA_clustered_res_", cluster_res,"_labeled.png"),height=1024,width=1024)
print(DimPlot(object = SC_integrated, reduction ="umap", label=T, repel=T, pt.size=2))
dev.off()



pdf(paste0("../plotting/plots/SC_integrated_DimPlots_UMAP_clustered.pdf"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="species"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="data_source"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T) +ggtitle("lizard cluster"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T)+ NoLegend() +ggtitle("lizard cluster"))
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="xie_clusters", label=T, repel=T)+ggtitle("Xie2021 cluster"))
print(DimPlot(object = SC_integrated, reduction ="umap", label=T))
dev.off()

SC_integrated_mus <- subset(SC_integrated, subset = species == "mouse")
SC_integrated_liz <- subset(SC_integrated, subset = species == "lizard")

Idents(SC_integrated_mus) <- SC_integrated_mus$integrated_snn_res.0.2
Idents(SC_integrated_liz) <- SC_integrated_liz$integrated_snn_res.0.2

SC_integrated_mus <- SCTransform(SC_integrated_mus)
SC_integrated_liz <- SCTransform(SC_integrated_liz,vars.to.regress=c("animal","chemistry"))

SC_integrated_mus.markers <- FindAllMarkers(SC_integrated_mus, only.pos = T, min.pct = 0.25, logfc.threshold = 0.25, assay='SCT')
saveRDS(SC_integrated_mus.markers, "../data/SC_integrated_mus.markers.rds")
SC_integrated_mus.markers_top25 <- SC_integrated_mus.markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(SC_integrated_mus.markers_top25, file = "../data/SC_integrated_mus.markers_top25.csv")

SC_integrated_liz.markers <- FindAllMarkers(SC_integrated_liz, only.pos = T, min.pct = 0.25, logfc.threshold = 0.25, assay='SCT')
saveRDS(SC_integrated_liz.markers, "../data/SC_integrated_liz.markers.rds")
SC_integrated_liz.markers_top25 <- SC_integrated_liz.markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(SC_integrated_liz.markers_top25, file = "../data/SC_integrated_liz.markers_top25.csv")


dat <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat) <- c("cluster","conserved_marker_gene")
dat_conc <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat_conc) <- c("cluster","conserved_marker_gene")

for (i in c(0:6,8)){
dat <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat) <- c("cluster","conserved_marker_gene")
cluster_markers_mus <- subset(SC_integrated_mus.markers, subset = cluster == i)
cluster_markers_liz <- subset(SC_integrated_liz.markers, subset = cluster == i)
common_genes <- intersect(cluster_markers_mus$gene,cluster_markers_liz$gene)
dat[1:length(common_genes),1] <- i
dat[,2] <- common_genes
dat_conc <- rbind(dat_conc, dat)

}

write.csv(dat_conc, file = "../data/SC_integrated_conserved_markers_by_clusters.csv")
dat_conc <- read.table("../data/SC_integrated_conserved_markers_by_clusters.csv", sep=",", header=T)


TFs <- scan(file = '../data/211101_TFs_mouse_pogona.txt', what=character())


conserved_TF_SC <- intersect(TFs,dat_conc$conserved_marker_gene)

dat_conc_TFs <- dat_conc[dat_conc$conserved_marker_gene %in% TFs,]
dat_conc_TFs$cluster_incr <- c(dat_conc_TFs$cluster+1)

pdf("../plotting/plots/SC_conserved_TFs_by_cluster_histogram.pdf")
hist(dat_conc_TFs$cluster)
dev.off()


dat_conc_TFs <- dat_conc[dat_conc$conserved_marker_gene %in% TFs,]
dat_conc_TFs$cluster_incr <- c(dat_conc_TFs$cluster+1)
pdf("../plotting/plots/SC_conserved_TFs_by_cluster_histogram.pdf")
hist(dat_conc_TFs$cluster_incr, breaks=c(0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5),col="grey",
main="Conserved TFs by cluster",
xlab="cluster",
ylab="# of conserved TFs",
ylim=c(0,12))
dev.off()

TF_list <- c("TCF7L2","EBF3","LHX9","LMO1","LMO3","LMO4","NFIB","NFIX","ST18","OTX2","MEIS2","SOX14","PAX7","GATA3","ZEB2","ZFHX4","IRX2","FAT1")

pdf("SC_integrated_clusters_DotPlot_TFs.pdf",width = 8)
DotPlot(SC_integrated_mus, features = rev(TF_list))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse integrated clusters")
DotPlot(SC_integrated_liz, features = rev(TF_list))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("lizard integrated clusters")
dev.off()

pdf("SC_integrated_clusters_DotPlot_conserved_TFs.pdf",width = 8)
DotPlot(SC_integrated_mus, features = rev(conserved_TF_SC))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse integrated clusters")
DotPlot(SC_integrated_liz, features = rev(conserved_TF_SC))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("lizard integrated clusters")
dev.off()


pdf("SC_integrated_clusters_DotPlot_conserved_TFs_integrated-plot.pdf",width = 8)
DotPlot(SC_integrated, features = rev(conserved_TF_SC), split.by="species",assay="SCT", cols=c("red","blue","lightgrey"))+theme(axis.text.x = element_text(angle = 90, hjust = 1)) +ggtitle("mouse + lizard integrated clusters")
dev.off()


Idents(SC_integrated_liz) <- SC_integrated_liz$integrated_snn_res.0.2


png(paste0("SC_integrated_DimPlot_species_UMAP_shuffled_transparent_pt_2.png"),height=1024,width=1024)
print(DimPlot(object = SC_integrated, reduction ="umap", group.by="species", cols=c(adjustcolor("#998ec3", alpha=0.5),adjustcolor("#f1a340", alpha=0.5)),label=F, pt.size=2, order=c("lizard","mouse")))
dev.off()

SC_integrated$incr_clusters <- c(as.numeric(as.character(SC_integrated$integrated_snn_res.0.2))+1)
Idents(SC_integrated) <- SC_integrated$incr_clusters


SC_integrated$incr_clusters <- c(as.numeric(as.character(SC_integrated$integrated_snn_res.0.2))+1)
Idents(SC_integrated) <- SC_integrated$incr_clusters

saveRDS(SC_integrated, file = "../data/SC_integrated_clustered.rds")
