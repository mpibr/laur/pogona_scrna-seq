library(Seurat)
library(dplyr)
library(corrplot)
library(zoo)
library(ggplot2)


### Lizard PCA-analysis


thal_glu_names <- c(paste0("DMT_",seq(1:4)),"MT/DMT","DLT",paste0("Rot_",seq(1:3)),paste0("MT_",seq(1:7)))
thal <- readRDS(file="../data/thalamus_subclustered.rds")
thal_glu <- subset(thal, idents=thal_glu_names)
thal_glu <- SCTransform(thal_glu, vars.to.regress = c("chemistry","animal"), return.only.var.genes = FALSE,variable.features.n=2000)
thal_glu <- RunPCA(thal_glu, npcs=100, features=VariableFeatures(thal_glu))

pdf("../plotting/plots/thal_glu_PC_Heatmap.pdf")
DimHeatmap(thal_glu, dims = 1:6, cells = 500, balanced = TRUE)
dev.off()

pdf("../plotting/plots/thal_glu_PC_dimplot.pdf")
DimPlot(thal_glu, reduction = "pca")
DimPlot(thal_glu, reduction = "pca", label=T,repel=T) + NoLegend()
dev.off()

saveRDS(thal_glu, "../data/thal_glu_PCA_calc.rds")



pca_loadings <- Loadings(thal_glu, reduction = "pca")[, 1]
pca_loadings_sorted <- sort(pca_loadings, decreasing=T)

write.csv(pca_loadings, file = "../plotting/plots/pca_loadings_lizard-thalamus.csv")
write.csv(pca_loadings_sorted, file = "../plotting/plots/pca_loadings_sorted_lizard-thalamus.csv")

expressed.genes <-rownames(thal_glu)

write(expressed.genes, file="../plotting/plots/lizard_glu_thal_expressed_genes.txt")

pca_loadings <- Loadings(thal_glu, reduction = "pca")[, 1]
pca_loadings <- names(pca_loadings)
write(pca_loadings, file="../data/lizard_glu_thal_pca_loadings_genes.txt")

### Mouse integration + PCA-analysis


thal_saunders <- readRDS("../data/Thalamus_saunders.rds")
thal_saunders <- subset(thal_saunders, subset = nFeature_RNA > 1500)
thal_saunders$data_source <- "Saunders"
thal_saunders$species <- "mouse"
Idents(thal_saunders) <- thal_saunders$subcluster
thal_saunders_glu_clusters <- c("2", paste0("2-", seq(from=1, to=11)))
thal_saunders <- subset(thal_saunders, idents=thal_saunders_glu_clusters)
#saveRDS(thal_saunders,"thal_saunders_1500_genes.rds")


l6_r2_cns_neurons <- readRDS(file="../data/l6_r2_cns_neurons_Seurat.rds")
l6_r2_cns_neurons <- AddMetaData(l6_r2_cns_neurons, metadata = Idents(l6_r2_cns_neurons), col.name='ClusterID')
linnarsson_thal_glu_clusters <- c(paste0("DEGLU", seq(from=1, to=4)))
##did not include DEINH4, possible mis-annotation by Linnarsson?
thal_linnarsson <- subset(l6_r2_cns_neurons, idents = linnarsson_thal_glu_clusters)
thal_linnarsson$data_source <- "Zeisel2018"
thal_linnarsson$species <- "mouse"
#saveRDS(thal_linnarsson,"linnarsson_thalamus_w-o_DEINH4.rds")
#thal_linnarsson <- readRDS("linnarsson_thalamus_w-o_DEINH4.rds")

thal_phillips <-  read.csv(file = "../data/GSE133912_thal_singlecell_counts.csv", stringsAsFactors = FALSE, row.names=1))
thal_phillips <- t(thal_phillips)
thal_phillips_metadata <- read.csv(file = '../data/GSE133912_thal_singlecell_metadata.csv', stringsAsFactors = FALSE, row.names=1)
thal_phillips <- CreateSeuratObject(counts = thal_phillips, meta.data = thal_phillips_metadata)
thal_phillips$data_source <- "Phillips2019"
thal_phillips$species <- "mouse"
Idents(thal_phillips) <- thal_phillips$Projection

#saveRDS(thal_phillips,"phillips_thal.rds")

saunders <- SCTransform(thal_saunders)
linnarsson <- SCTransform(thal_linnarsson)
phillips <- SCTransform(thal_phillips)



thal.list <- list(saunders,linnarsson,phillips)

nfeatures <- 2000
nDims_CCA <- 25

features <- SelectIntegrationFeatures(object.list = thal.list, nfeatures = nfeatures)
thal.list <- PrepSCTIntegration(object.list = thal.list, anchor.features = features)


thal_anchors <- FindIntegrationAnchors(object.list = thal.list, anchor.features= features, normalization.method = "SCT", dims = 1:nDims_CCA, reduction="cca")
thal_integrated <- IntegrateData(anchorset = thal_anchors, normalization.method = "SCT", dims = 1:nDims_CCA)

thal_integrated <- RunPCA(thal_integrated, npcs = 100, verbose = T)
pdf("../plotting/plots/thal_integrated_Elbowplot_",nfeatures,".pdf")
print(ElbowPlot(object = thal_integrated, ndims = 100))
dev.off()

nDims_PCA <- 25

thal_integrated <- RunUMAP(thal_integrated, reduction = "pca", dims = 1:nDims_PCA)
thal_integrated <- FindNeighbors(object = thal_integrated, dims=1:nDims_PCA)

pdf(paste0("../plotting/plots/thal_integrated_DimPlots_UMAP_",nfeatures,"_features_",nDims_PCA,"_PCs.pdf"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="data_source"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="species"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="ClusterID", label=T, repel=T)+ggtitle("Zeisel2018 cluster"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="subcluster", label=T, repel=T)+ggtitle("Saunders2018 cluster"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="joint_cluster", label=T, repel=T)+ggtitle("Phillips2019 joint_cluster"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="Projection", label=T, repel=T)+ggtitle("Phillips2019 projection"))
print(DimPlot(object = thal_integrated, reduction ="umap", group.by="cluster", label=T, repel=T) + NoLegend()+ggtitle("Phillips2019 cluster"))
dev.off()


saveRDS(thal_integrated, file ="../data/thalamus_mouse_integrated.rds")

pca_loadings <- Loadings(thal_integrated, reduction = "pca")[, 1]
pca_loadings_sorted <- sort(pca_loadings, decreasing=T)

write.csv(pca_loadings, file = "../data/pca_loadings_mouse-thal-integrated.csv")
write.csv(pca_loadings_sorted, file = "../data/pca_loadings_sorted_mouse-thal-integrated.csv")

common.genes <- union(rownames(saunders),rownames(linnarsson))
common.genes <- union(common.genes,rownames(phillips))

write(common.genes, file="../data/common_mouse_genes_expressed.txt")


pca_loadings <- Loadings(thal_integrated, reduction = "pca")[, 1]
pca_loadings <- names(pca_loadings)
write(pca_loadings, file="../data/thal_mouse_integrated_pca_loadings_genes.txt")


liz_glu_thal <- readRDS(file="../data/thal_glu_PCA_calc.rds")
mus_glu_thal <- readRDS(file ="../data/thalamus_mouse_integrated.rds")

liz_pca_loadings <- Loadings(liz_glu_thal, reduction = "pca")[, 1]
liz_pca_loadings_sorted <- sort(liz_pca_loadings, decreasing=T)

mus_pca_loadings <- Loadings(mus_glu_thal, reduction = "pca")[, 1]
mus_pca_loadings_sorted <- sort(mus_pca_loadings, decreasing=T)

#orthologous genes
one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
colnames(one2one) <- c("pogona","orthology","mouse")
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)
#removing duplicates
one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

mouse_to_lizard_gene_names <- function(mouse_gene_names) {
  lizard_gene_names <- c()
  for (i in 1:length(mouse_gene_names)){
    o2o_gene <- one2one[one2one$mouse==mouse_gene_names[i],]
    gene <- o2o_gene$pogona
    lizard_gene_names <- c(lizard_gene_names,gene)
  }
  return(lizard_gene_names)
}

lizard_to_mouse_gene_names <- function(lizard_gene_names) {
  mouse_gene_names <- c()
  for (i in 1:length(lizard_gene_names)){
    o2o_gene <- one2one[one2one$pogona==lizard_gene_names[i],]
    gene <- o2o_gene$mouse
    mouse_gene_names <- c(mouse_gene_names,gene)
  }
  return(mouse_gene_names)
}

liz_pca_loadings_o2o <- liz_pca_loadings[names(liz_pca_loadings) %in% one2one$pogona]
liz_pca_loadings_o2o_sorted <- sort(abs(liz_pca_loadings_o2o), decreasing=T)
names(liz_pca_loadings_o2o_sorted) <- lizard_to_mouse_gene_names(names(liz_pca_loadings_o2o_sorted))

mus_pca_loadings_o2o <- mus_pca_loadings[names(mus_pca_loadings) %in% one2one$mouse]
mus_pca_loadings_o2o_sorted <- sort(abs(mus_pca_loadings_o2o), decreasing=T)

mus_top200_loadings <- names(mus_pca_loadings_o2o_sorted)[1:200]
liz_top200_loadings <- names(liz_pca_loadings_o2o_sorted)[1:200]

mus_top400_loadings <- names(mus_pca_loadings_o2o_sorted)[1:400]
liz_top400_loadings <- names(liz_pca_loadings_o2o_sorted)[1:400]

common_lizard_mus_top200 <- intersect(mus_top200_loadings, liz_top200_loadings)
common_lizard_mus_top400 <- intersect(mus_top400_loadings, liz_top400_loadings)

write(common_lizard_mus_top200, file="../data/common_lizard-mouse_glutamatergic_thalamus_PC1_genes_top200.txt")
write(common_lizard_mus_top400, file="../data/common_lizard-mouse_glutamatergic_thalamus_PC1_genes_top400.txt")

mouse.genes <- read.delim(file="../data/common_mouse_genes_expressed.txt")
mouse.genes <- mouse.genes[,"V1"]
lizard.genes <- read.delim(file="../data/lizard_glu_thal_expressed_genes.txt", header=F)
lizard.genes <- lizard.genes[,"V1"]

mouse.genes.o2o <- mouse.genes[mouse.genes %in% one2one$mouse]
lizard.genes.o2o <- lizard.genes[lizard.genes %in% one2one$pogona]
lizard.genes.o2o.mouse_names <- lizard_to_mouse_gene_names(lizard.genes.o2o)

background <- intersect(mouse.genes.o2o,lizard.genes.o2o.mouse_names)
write(background, file="../data/background_lizard-mouse_glutamatergic_thalamus_expressed_genes.txt")

#top400
universe <- background
diffexpr <- common_lizard_mus_top400
universe <- toupper(universe)
diffexpr <- toupper(diffexpr)



gostres <- gost(query = diffexpr,
                organism = "mmusculus", ordered_query = FALSE,
                multi_query = FALSE, significant = TRUE, exclude_iea = FALSE,
                measure_underrepresentation = FALSE, evcodes = FALSE,
                user_threshold = 0.01, correction_method = "g_SCS",
                domain_scope = "annotated", custom_bg = universe,
                numeric_ns = "",  as_short_link = FALSE,sources = c("GO:MF", "GO:BP","GO:CC"))

gostplot(gostres, capped = FALSE, interactive = TRUE)



result_df <- gostres$result
result_df$GO_term <- paste(result_df$term_id,result_df$term_name)

library(dplyr)

result_df_top10 <- result_df %>% group_by(source) %>% top_n(10, -p_value)

result_df_top10$GO_term <- factor(result_df_top10$GO_term, levels = result_df_top10$GO_term[order(-log10(result_df_top10$p_value))])

pdf("../plotting/plots/GO-analysis_top400_PC1_glutamatergic_thalamus.pdf")
ggplot(result_df_top10, aes(x=GO_term, y=-log10(p_value), fill=source)) +     geom_bar(stat="identity")  + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()

pdf("../plotting/plots/GO-analysis_top400_PC1_glutamatergic_thalamus_bw.pdf")
ggplot(result_df_top10, aes(x=GO_term, y=-log10(p_value), fill=source)) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()

result_mf_top10 <- result_df_top10[result_df_top10$source=="GO:MF",]
result_cc_top10 <- result_df_top10[result_df_top10$source=="GO:CC",]
result_bp_top10 <- result_df_top10[result_df_top10$source=="GO:BP",]



pdf("../plotting/plots/GO-analysis_top400_PC1_glutamatergic_thalamus_bw_GO-MF.pdf")
ggplot(result_mf_top10, aes(x=GO_term, y=-log10(p_value))) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()

pdf("../plotting/plots/GO-analysis_top400_PC1_glutamatergic_thalamus_bw_GO-CC.pdf")
ggplot(result_cc_top10, aes(x=GO_term, y=-log10(p_value))) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()

pdf("../plotting/plots/GO-analysis_top400_PC1_glutamatergic_thalamus_bw_GO-BP.pdf")
ggplot(result_bp_top10, aes(x=GO_term, y=-log10(p_value))) +     geom_bar(stat="identity") + theme_bw() + theme(axis.text.x = element_text(angle = 270,hjust=0))
dev.off()
