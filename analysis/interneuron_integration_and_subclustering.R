library(future)
plan("multiprocess", workers = 12)
options(future.globals.maxSize= 94371840000) #

library(Seurat)
library(dplyr)
library(cowplot)
library(zoo)
library(ggplot2)
library(scater)
library(stringr)
library(corrplot)



#reading in GABAergic neurons
neurons <- readRDS(file="../data/neurons_SCTransform_annotated.rds")
liz_INs <- subset(neurons, idents=c(paste0("TEGABA", seq(1:24))))

liz_INs$cluster_annotation <- Idents(liz_INs)

liz_INs <- SCTransform(liz_INs, vars.to.regress=c("animal"), return.only.var.genes = FALSE)


liz_INs <- RunPCA(liz_INs, verbose = FALSE, npcs = 100)

pdf("../plotting/plots/Elbowplot_liz_INs_SCT.pdf")
ElbowPlot(liz_INs, ndims=100)
dev.off()

liz_INs <- RunUMAP(liz_INs, dims = 1:50, verbose = FALSE)

liz_INs <- FindNeighbors(liz_INs, dims = 1:50, verbose = FALSE)
liz_INs <- FindClusters(liz_INs, verbose = FALSE, resolution=2)

pdf("../plotting/plots/DimPlot_liz_INs_SCT.pdf")
DimPlot(liz_INs, label = TRUE) + NoLegend()
DimPlot(liz_INs, label = TRUE)
DimPlot(liz_INs, reduction = "umap", label = TRUE, group.by="cluster_annotation")  + NoLegend()
DimPlot(liz_INs, reduction = "umap", label = TRUE, group.by="cluster_annotation")
DimPlot(liz_INs, reduction = "umap", group.by="chemistry")
FeaturePlot(object = liz_INs, feature ="nCount_RNA")
FeaturePlot(object = liz_INs, feature ="nFeature_RNA")
dev.off()


liz_INs.markers <- FindAllMarkers(liz_INs, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25, assay="SCT")
saveRDS(liz_INs.markers,"../data/liz_INs.markers.rds")
top10_SCT <- liz_INs.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC)
top25_SCT <- liz_INs.markers %>% group_by(cluster) %>% top_n(n = 25, wt = avg_logFC)
write.csv(top10_SCT, file = "../data/liz_INs_SCT.markers_top10.csv")
write.csv(top25_SCT, file = "../data/liz_INs_SCT.markers_top25.csv")

liz_INs$subclusters <- Idents(liz_INs)
saveRDS(liz_INs,'../data/liz_INs.rds')


sub_genes <- c("SLC32A1","GAD1", "GAD2","ZEB2","LHX6","ADARB2","ETV1","CCK","RELN","CHODL","GPR151","PENK","PROK1","NOS1","LOC110070181","LOC110073234","CNR1","SIK1","SYT2","ECEL1","CRH","PROKR1","LOC110073245","MEIS2","TRH", "FOXP2")
pdf("../plotting/plots/liz_INs_markers.pdf",width = 10, height=10)
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()

Idents(liz_INs) <- liz_INs$subclusters
levels(liz_INs) <- c(0:37)

sub_genes <- c("SLC32A1","GAD1", "GAD2","ZEB2","LHX6","ADARB2","ETV1","CCK","RELN","CHODL","GPR151","PENK","PROK1","NOS1","LOC110070181","LOC110073234","CNR1","SIK1","SYT2","ECEL1","CRH","PROKR1","LOC110073245","MEIS2","TRH", "FOXP2")
pdf("../plotting/plots/liz_INs_markers.pdf",width = 10, height=10)
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()

Idents(liz_INs) <- liz_INs$cluster_annotation
levels(liz_INs) <- c(paste0("TEGABA", seq(1:24)))

sub_genes <- c("SLC32A1","GAD1", "GAD2","ZEB2","LHX6","ADARB2","ETV1","CCK","RELN","CHODL","GPR151","PENK","PROK1","NOS1","LOC110070181","LOC110073234","CNR1","SIK1","SYT2","ECEL1","CRH","PROKR1","LOC110073245","MEIS2","TRH", "FOXP2","TH","TAC1","DRD1","DRD2","FOXP1","SATB2","SOX6","LAMP5","SP8","ZIC1","ZIC2","NKX2-1","LHX5","LHX8","LHX9")
Idents(liz_INs) <- liz_INs$cluster_annotation
levels(liz_INs) <- c(paste0("TEGABA", seq(1:24)))

pdf("../plotting/plots/liz_INs_markers_all_clusters.pdf",width = 10, height=10)
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
Idents(liz_INs) <- liz_INs$subclusters
levels(liz_INs) <- c(0:37)
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()

sub_genes <- c("SLC32A1","GAD1", "GAD2","ZEB2","LHX6","ADARB2","ETV1","CCK","RELN","CHODL","GPR151","PENK","PROK1","NOS1","LOC110070181","LOC110073234","CNR1","SIK1","SYT2","ECEL1","CRH","PROKR1","LOC110073245","MEIS2","TRH", "FOXP2","TH","TAC1","DRD1","DRD2","FOXP1","SATB2","SOX6","LAMP5","SP8","ZIC1","ZIC2","NKX2-1","LHX5","LHX8","LHX9")
Idents(liz_INs) <- liz_INs$cluster_annotation
levels(liz_INs) <- c(paste0("TEGABA", seq(1:24)))

pdf("../plotting/plots/liz_INs_markers_all_clusters_ordered_hclust.pdf",width = 10, height=10)
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
Idents(liz_INs) <- liz_INs$subclusters
levels(liz_INs) <- ordered_labels
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()


liz_INs <- RenameIdents(liz_INs, '0'="MGE_ZEB2_LHX6_ETV1_ST18")
liz_INs <- RenameIdents(liz_INs, '1'="LGE_ZIC1_RASD1_CLIC4")
liz_INs <- RenameIdents(liz_INs, '2'="CGE_ZEB2_ADARB2_CCK_CPNE7")
liz_INs <- RenameIdents(liz_INs, '3'="MGE_ZEB2_LHX6_ETV1_PROK1_CORT")
liz_INs <- RenameIdents(liz_INs, '4'="MGE_ZEB2_LHX6_RELN_COL25A1")
liz_INs <- RenameIdents(liz_INs, '5'="MGE_ZEB2_LHX6_ETV1_PROK1_NXPH1")
liz_INs <- RenameIdents(liz_INs, '6'="MGE_ZEB2_LHX6_NPY_PKIB")
liz_INs <- RenameIdents(liz_INs, '7'="LGE_MEIS2_TAC3_GSG1L2")
liz_INs <- RenameIdents(liz_INs, '8'="CGE_ZEB2_ADARB2_PROX1_CRTAC1")
liz_INs <- RenameIdents(liz_INs, '9'="LGE_MEIS2_TAC1_PDYN_VIP")
liz_INs <- RenameIdents(liz_INs, '10'="LGE_MEIS2_FOXP2_CCK_TSHZ1")
liz_INs <- RenameIdents(liz_INs, '11'="LGE_ZIC1_SIX3_TPPP3")
liz_INs <- RenameIdents(liz_INs, '12'="MGE_ZEB2_LHX6_LAMP5_SLC29A1")
liz_INs <- RenameIdents(liz_INs, '13'="LGE_MEIS2_PENK_CARHSP1")
liz_INs <- RenameIdents(liz_INs, '14'="MGE_LHX6_RELN_CHODL_SRPX")
liz_INs <- RenameIdents(liz_INs, '15'="CGE_ZEB2_ADARB2_CCK")
liz_INs <- RenameIdents(liz_INs, '16'="LGE_MEIS2_FOXP2_CCK_TSHZ1_VWA1")
liz_INs <- RenameIdents(liz_INs, '17'="LGE_MEIS2_PCP4_TLE4")
liz_INs <- RenameIdents(liz_INs, '18'="CGE_ZEB2_ADARB2_RELN_RGS12")
liz_INs <- RenameIdents(liz_INs, '19'="non-TEGABA?_SST2_NPY_FIGN_VAT1")
liz_INs <- RenameIdents(liz_INs, '20'="MGE_LHX6_NOS1_CHODL")
liz_INs <- RenameIdents(liz_INs, '21'="LGE_ZEB2_ZIC1_FOSL2_HTR1B")
liz_INs <- RenameIdents(liz_INs, '22'="MGE_ZEB2_LHX6_ETV1_GABRD")
liz_INs <- RenameIdents(liz_INs, '23'="LGE_MEIS2_TAC3_TMEM61")
liz_INs <- RenameIdents(liz_INs, '24'="MGE_ZEB2_LHX6_LAMP5_KCTD6")
liz_INs <- RenameIdents(liz_INs, '25'="MGE_ZEB2_LHX6_ETV1_SST2_LHX8_ST18")
liz_INs <- RenameIdents(liz_INs, '26'="LGE_ZIC1_PYY_SALL3")
liz_INs <- RenameIdents(liz_INs, '27'="MGE_ZEB2_LHX6_ETV1_NOS1")
liz_INs <- RenameIdents(liz_INs, '28'="non-TEGABA_ONECUT3")
liz_INs <- RenameIdents(liz_INs, '29'="MGE_ZEB2_LHX6_RELN_CDH23")
liz_INs <- RenameIdents(liz_INs, '30'="LGE_ZEB2_MEIS2_CRIP1_TRH")
liz_INs <- RenameIdents(liz_INs, '31'="non-TEGABA_OTP_GHR")
liz_INs <- RenameIdents(liz_INs, '32'="CGE_ZEB2_ADARB2_RELN_DNTT")
liz_INs <- RenameIdents(liz_INs, '33'="MGE_ZEB2_LHX6_SNCG_CRHBP")
liz_INs <- RenameIdents(liz_INs, '34'="non-TEGABA?_TAC3_MC4R")
liz_INs <- RenameIdents(liz_INs, '35'="LGE_ZIC1_SEC14L5_CLIC4")
liz_INs <- RenameIdents(liz_INs, '36'="MGE_ZEB2_LHX6_LAMP5_SCUBE1")
liz_INs <- RenameIdents(liz_INs, '37'="non-TEGABA_TH_DDC_SATB2")

pdf("../plotting/plots/DimPlot_liz_INs_SCT_new_annotation.pdf")
DimPlot(liz_INs, label = TRUE, label.size = 2, repel=T) + NoLegend()
DimPlot(liz_INs, label = TRUE)
DimPlot(liz_INs, reduction = "umap", label = TRUE, group.by="cluster_annotation")  + NoLegend()
DimPlot(liz_INs, reduction = "umap", label = TRUE, group.by="cluster_annotation")
DimPlot(liz_INs, reduction = "umap", group.by="chemistry")
FeaturePlot(object = liz_INs, feature ="nCount_RNA")
FeaturePlot(object = liz_INs, feature ="nFeature_RNA")
dev.off()


liz_INs.markers <- FindAllMarkers(liz_INs, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25, assay="SCT")
saveRDS(liz_INs.markers,"../data/liz_INs.markers_new_annotation.rds")
top10_SCT <- liz_INs.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC)
top25_SCT <- liz_INs.markers %>% group_by(cluster) %>% top_n(n = 25, wt = avg_logFC)
write.csv(top10_SCT, file = "../data/liz_INs_SCT.markers_top10_new_annotation.csv")
write.csv(top25_SCT, file = "../data/liz_INs_SCT.markers_top25_new_annotation.csv")

liz_INs_avg <- AverageExpression(liz_INs)
liz_INs_avg_data <- liz_INs_avg[["SCT"]]

liz_INs_cor <- cor(liz_INs_avg_data, method="s")
liz_INs_dist <- as.dist(1- liz_INs_cor)
liz_INs_dendrogram <- hclust(liz_INs_dist, method = "ward.D2")
ordered_labels <- liz_INs_dendrogram[["labels"]][liz_INs_dendrogram[["order"]]]
levels(liz_INs) <- ordered_labels

pdf("../plotting/plots/lizard_interneurons_dendrogram.pdf",width=40,height=25)
plot(liz_INs_dendrogram, hang=-1)
dev.off()

sub_genes <- unique(c("SLC32A1","GAD1", "GAD2","ZEB2","LHX6","ADARB2","ETV1","MEIS2","ZIC1","CCK","RELN","CHODL","GPR151","PENK","PROK1","NOS1","LOC110070181","LOC110073234","CNR1","SIK1","SYT2","ECEL1","CRH","PROKR1","LOC110073245","TRH", "FOXP2","TH","TAC1","DRD1","DRD2","FOXP1","SATB2","SOX6","LAMP5","SP8","ZIC1","ZIC2","NKX2-1","LHX5","LHX8","LHX9"))

pdf("../plotting/plots/liz_INs_markers_all_clusters_ordered_hclust.pdf",width = 20, height=10)
DotPlot(liz_INs, features = rev(sub_genes))+theme(axis.text.x = element_text(angle = 90, hjust = 1))
dev.off()
liz_INs$subclusters <- Idents(liz_INs)

liz_INs <- subset(liz_INs, idents=c("non-TEGABA_ONECUT3","non-TEGABA_OTP_GHR","non-TEGABA?_TAC3_MC4R","non-TEGABA_TH_DDC_SATB2"), invert=T)
saveRDS(liz_INs,'../data/liz_INs_wo_non_TEGABA.rds')


liz_MGE_CGE <- c("MGE_ZEB2_LHX6_ETV1_PROK1_CORT","MGE_ZEB2_LHX6_ETV1_PROK1_NXPH1","MGE_ZEB2_LHX6_RELN_CDH23","MGE_ZEB2_LHX6_RELN_COL25A1","MGE_ZEB2_LHX6_NPY_PKIB","MGE_LHX6_RELN_CHODL_SRPX","MGE_LHX6_NOS1_CHODL","MGE_ZEB2_LHX6_SNCG_CRHBP","CGE_ZEB2_ADARB2_CCK_CPNE7","CGE_ZEB2_ADARB2_CCK","CGE_ZEB2_ADARB2_PROX1_CRTAC1","CGE_ZEB2_ADARB2_RELN_RGS12","CGE_ZEB2_ADARB2_RELN_DNTT","MGE_ZEB2_LHX6_LAMP5_SLC29A1","MGE_ZEB2_LHX6_LAMP5_KCTD6","MGE_ZEB2_LHX6_ETV1_NOS1","MGE_ZEB2_LHX6_ETV1_GABRD","MGE_ZEB2_LHX6_LAMP5_SCUBE1")
liz_TEGABA <- subset(liz_INs, idents=liz_MGE_CGE)
liz_TEGABA <- subset(liz_TEGABA, subset = chemistry == "v3")
liz_TEGABA$subclusters <- Idents(liz_TEGABA)
liz_TEGABA$species <- "lizard"
liz_TEGABA$data_source <- "lizard"
saveRDS(liz_TEGABA,'../data/liz_TEGABA.rds')

allen_INs <- readRDS('../data/ABI_cortex_small_GABAergic_seurat.rds')
allen_INs$species <- "mouse"
allen_INs$data_source <- "Yao2021"
allen_INs <- allen_INs[, sample(colnames(allen_INs), size =8000, replace=F)]
saveRDS(allen_INs,'../data/allen_INs.rds')

#one to one orthologs
one2one <- read.table("../data/190501_ENSEMBL_Pogona_Mouse_orthologs.txt", sep=",", header=T)
colnames(one2one) <- c("pogona","orthology","mouse")
one2one <-one2one[one2one$orthology=="ortholog_one2one",]
one2one$pogona <- as.character(one2one$pogona)
one2one$mouse <- as.character(one2one$mouse)
one2one <- subset(one2one, !duplicated(one2one$pogona))
one2one <- subset(one2one, !duplicated(one2one$mouse))

allen_INs.raw <- GetAssayData(allen_INs)
allen_INs.raw <- allen_INs.raw[rownames(allen_INs.raw) %in% one2one$mouse,]

one2one.allen_INs <- one2one[one2one$mouse %in% rownames(allen_INs.raw),]
one2one.allen_INs <- one2one.allen_INs[order(match(one2one.allen_INs$mouse, rownames(allen_INs.raw))),]
all.equal(one2one.allen_INs$mouse, rownames(allen_INs.raw))
# true
rownames(allen_INs.raw) <- one2one.allen_INs$pogona


liz.raw <- GetAssayData(liz_TEGABA)
liz.raw <- liz.raw[rownames(liz.raw) %in% one2one.allen_INs$pogona,]

dim(liz.raw)

common.genes <- intersect(rownames(allen_INs.raw),rownames(liz.raw))

length(common.genes)

allen_INs.raw <- allen_INs.raw[rownames(allen_INs.raw) %in% common.genes,]
liz.raw <- liz.raw[rownames(liz.raw) %in% common.genes,]

dim(allen_INs.raw)
# [1] 10909 20070
dim(liz.raw)
# [1] 10909 5706

#integrated analysis
allen_INs_metadata <- allen_INs@meta.data
liz_metadata <- liz_TEGABA@meta.data

allen_INs <- CreateSeuratObject(allen_INs.raw)
liz_TEGABA <- CreateSeuratObject(liz.raw)

allen_INs <- AddMetaData(allen_INs, metadata = allen_INs_metadata)
liz_TEGABA <- AddMetaData(liz_TEGABA, metadata = liz_metadata)

nDims_PCA <- 30
nVarGenes <- 1500
nDims_CCA <- 20

allen_INs <- SCTransform(allen_INs, return.only.var.genes = FALSE)
liz_TEGABA <- SCTransform(liz_TEGABA,vars.to.regress=c("animal"), return.only.var.genes = FALSE)

int_features <- SelectIntegrationFeatures(object.list = INs.list, nfeatures = nVarGenes)
INs.list.loop <- PrepSCTIntegration(object.list = INs.list, anchor.features = int_features)


INs_anchors <- FindIntegrationAnchors(object.list = INs.list.loop, anchor.features= int_features, normalization.method = "SCT", dims = 1:nDims_CCA, reduction="cca")
INs_integrated <- IntegrateData(anchorset = INs_anchors, normalization.method = "SCT", dims = 1:nDims_CCA)


INs_integrated <- RunPCA(INs_integrated, npcs = 100, verbose = FALSE)
pdf(paste0("../plotting/plots/INs_integrated_Elbowplot_", nVarGenes,"VG_", nDims_CCA,"_CCs.pdf"))
print(ElbowPlot(object = INs_integrated, ndims = 100))
dev.off()
INs_integrated <- RunUMAP(INs_integrated, reduction = "pca", dims = 1:nDims_PCA)

pdf(paste0("../plotting/plots/INs_integrated_DimPlots_UMAP_", nVarGenes,"VG_", nDims_CCA,"_CCs_",nDims_PCA,"_PCs_SCTransform.pdf"))
#print(DimPlot(object = INs_integrated, reduction ="umap", label=T, repel=T)+ NoLegend())
print(DimPlot(object = INs_integrated, reduction ="umap", group.by="species"))
print(DimPlot(object = INs_integrated, reduction ="umap", group.by="data_source"))
print(DimPlot(object = INs_integrated, reduction ="umap", group.by="cluster_annotation", label=T, repel=T)+ NoLegend() +ggtitle("lizard cluster"))
print(DimPlot(object = INs_integrated, reduction ="umap", group.by="subclusters", label=T, repel=T, label.size = 2)+ NoLegend() +ggtitle("lizard subcluster"))
print(DimPlot(object = INs_integrated, reduction ="umap", group.by="cluster_label", label=T, repel=T, label.size = 2.5)+ NoLegend()+ggtitle("Yao2021 cluster"))
print(DimPlot(object = INs_integrated, reduction ="umap", group.by="subclass_label", label=T, repel=T, label.size = 2.5)+ NoLegend()+ggtitle("Yao2021 subclass"))

print(DimPlot(object = INs_integrated, reduction ="umap", group.by="chemistry", label=T, repel=T)+ NoLegend()+ggtitle("lizard chemistry"))
FeaturePlot(object = INs_integrated, feature ="nCount_RNA")
FeaturePlot(object = INs_integrated, feature ="nFeature_RNA")
dev.off()


INs_integrated <- FindNeighbors(object = INs_integrated, dims=1:nDims_PCA)
INs_integrated <- FindClusters(object = INs_integrated, resolution=0.15)
INs_integrated$integrated_clusters <- Idents(INs_integrated)



saveRDS(INs_integrated, file = "../data/INs_integrated_clustered.rds")

INs_integrated_mus <- subset(INs_integrated, subset = species == "mouse")
INs_integrated_liz <- subset(INs_integrated, subset = species == "lizard")

Idents(allen_INs) <- allen_INs$integrated_clusters
Idents(liz_TEGABA) <- liz_TEGABA$integrated_clusters

allen_INs.markers <- FindAllMarkers(allen_INs, only.pos = F, min.pct = 0.25, logfc.threshold = 0.25, assay='SCT')
saveRDS(allen_INs.markers, "../data/allen_INs_integrated_clusters.markers_also_negative.rds")
allen_INs.markers_top25 <- allen_INs.markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(allen_INs.markers_top25, file = "../data/allen_INs_integrated_clusters.markers_also_negative_top25.csv")


liz_TEGABA.markers <- FindAllMarkers(liz_TEGABA, only.pos = F, min.pct = 0.25, logfc.threshold = 0.25, assay='SCT')
saveRDS(liz_TEGABA.markers, "../data/liz_TEGABA_integrated_clusters.markers_also_negative.rds")
liz_TEGABA.markers_top25 <- liz_TEGABA.markers %>% group_by(cluster) %>% top_n(25, avg_logFC)
write.csv(liz_TEGABA.markers_top25, file = "../data/liz_TEGABA_integrated_clusters.markers_also_negative_top25.csv")


dat <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat) <- c("cluster","conserved_marker_gene")
dat_conc <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat_conc) <- c("cluster","conserved_marker_gene")

for (i in c(0:4,6)){
dat <- as.data.frame(matrix(ncol=2, nrow=0))
colnames(dat) <- c("cluster","conserved_marker_gene")
cluster_markers_mus <- subset(allen_INs.markers, subset = cluster == i)
cluster_markers_liz <- subset(liz_TEGABA.markers, subset = cluster == i)
common_genes <- intersect(cluster_markers_mus$gene,cluster_markers_liz$gene)
dat[1:length(common_genes),1] <- i
dat[,2] <- common_genes
dat_conc <- rbind(dat_conc, dat)

}

write.csv(dat_conc, file = "../data/INs_conserved_markers_by_clusters_also_negative.csv")

TFs <- scan(file = '../data/211101_TFs_mouse_pogona.txt', what=character())
conserved_TF_INs <- intersect(TFs,dat_conc$conserved_marker_gene)

dat_conc_TFs <- dat_conc[dat_conc$conserved_marker_gene %in% TFs,]
dat_conc_TFs$cluster_incr <- c(dat_conc_TFs$cluster+1)
pdf("../plotting/plots/INs_conserved_TFs_by_cluster_histogram.pdf")
hist(dat_conc_TFs$cluster_incr, breaks=c(0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5),col="grey",
main="Conserved TFs by cluster",
xlab="cluster",
ylab="# of conserved TFs"),
ylim=c(0,12))
dev.off()

INs_integrated_red <- subset(INs_integrated, idents=c(6,8,9), invert=T)
saveRDS(INs_integrated_red, file="INs_integrated_red.rds")
