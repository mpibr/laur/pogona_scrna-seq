# Pogona scRNA-seq analysis

Analysis pipeline for the data scRNA-seq data from _Pogona vitticeps_ brain reported in:

**'Molecular diversity and evolution of neuron types in the amniote brain'** by David Hain, Tatiana Gallego-Flores, Michaela Klinkmann, Angeles Macias, Elena Ciirdaeva, Anja Arends, Christina Thum, Georgi Tushev, Friedrich Kretschmer, Maria Antonietta Tosches and Gilles Laurent

## Contents


* **./analysis/**

Contains code used for the analysis. See ./analysis/readme.md for details.

* **./plotting/**

Contains code used for the plotting of the figures and a subfolder for plots. 

* **./data/**

Contains data used for the analysis and output data of scripts goes there.


## Downloads/Data accessibility
Sequencing data have been deposited in the NCBI Sequence Read Archive (BioProject PRJNA812380, previously published data (Norimoto*, Fenk* et al. 2020) BioProject PRJNA591493). 

Processed gene expression data can also be explored interactively here: https://public.brain.mpg.de/pogona2022.html

## Requirements
The scripts in ./analysis/ and ./plotting/ require the following software and packages:

	R version 3.6.3 (2020-02-29)
	Seurat_3.1.0
	sctransform_0.2.0
	ggplot2_3.2.1
	pvclust_2.2-0 
	corrplot_0.84
	matrixStats_0.56.0
	dplyr_0.8.3
	dendextend_1.14.0
	e1071_1.7-3
	cowplot_1.0.0
	future_1.16.0
	zoo_1.8-6
	loomR_0.2.1.9000
	stringr_1.4.0
	gprofiler2_0.2.1
	irlba_2.3.3


